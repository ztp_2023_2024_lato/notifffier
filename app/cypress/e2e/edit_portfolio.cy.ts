import { TestControllerApi } from "NotifffierApi";

describe('template spec', () => {
  it('passes', async () => {
    let formData = new FormData();
    formData.append("amount", 10);
    formData.append("rounds", 2);
    await fetch('/api/debug/purge', {method: 'POST', body: new FormData()} as any);
    await fetch('/api/debug/generateUsers', {method: 'POST', body: new FormData()} as any);
    await fetch('/api/debug/generateProducts', {method: 'POST', body: new FormData()} as any);
    await fetch('/api/debug/generatePortfolios', {method: 'POST', body: new FormData()} as any);
    let summary = await fetch('/api/debug/summary');
    let json = await summary.json();
    cy.visit(`/user/${json.userWithMostPortfolios}/portfolios`);
    cy.get('.btn').first().click();
    cy.get('#name').type('nowa nazwa');
    cy.get('#submit').click();
    cy.get('h3').contains('nowa nazwa');
  })
})
