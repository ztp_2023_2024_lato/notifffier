describe('template spec', () => {
  it('passes', () => {
    cy.visit('/');
    cy.contains("⚙").click();
    cy.url().should('include', '/admin');
  })
})
