describe('template spec', () => {
  it('passes', async () => {
    await fetch('/api/debug/purge', {method: 'POST'});
    let response = await fetch('/api/debug/summary');
    let summary = await response.json();
    expect(summary.numberOfAlerts).to.equal(0);
    expect(summary.numberOfMonitors).to.equal(0);
    expect(summary.numberOfPortfolios).to.equal(0);
    expect(summary.numberOfUsers).to.equal(0);
    expect(summary.userWithMostPortfolios).to.be.null;

    let formData = new FormData();
    formData.append("amount", 10);
    formData.append("rounds", 2);
    await fetch('/api/debug/generateUsers', {method: 'POST', body: formData} as any);
    await fetch('/api/debug/generateProducts', {method: 'POST', body: formData} as any);
    await fetch('/api/debug/generatePortfolios', {method: 'POST', body: formData} as any);

    response = await fetch('/api/debug/summary');
    summary = await response.json();
    console.log(summary);
    expect(summary.userWithMostPortfolios).not.to.be.null;
    expect(summary.userWithMostPortfolios).not.to.be.undefined;

    await fetch('/api/debug/purge', {method: 'POST'});

    response = await fetch('/api/debug/summary');
    summary = await response.json();
    expect(summary.numberOfAlerts).to.equal(0);
    expect(summary.numberOfMonitors).to.equal(0);
    expect(summary.numberOfPortfolios).to.equal(0);
    expect(summary.numberOfUsers).to.equal(0);
    expect(summary.userWithMostPortfolios).to.be.null;
  })
})
