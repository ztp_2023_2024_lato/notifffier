import * as React from 'react';
import './App.css';
import AppNavbar from './AppNavbar';
import {Link, useParams} from 'react-router-dom';
import { Button, Container, Row } from 'reactstrap';
import { PortfolioControllerApi, PortfolioDTO } from 'NotifffierApi';
import { useState } from 'react';
import ProductList from './ProductList';
import ApiConfig from './config';

const PortfolioApi = new PortfolioControllerApi(ApiConfig);

const PortfolioOverview = () => {
  const { userId } = useParams();

  const [portfolios, setPortfolios] = useState<PortfolioDTO[]>([]);
  const [loading, setLoading] = useState(false);
  const [numberOfPages, setNumberOfPages] = useState(1);
  const [pageNumber, setPageNumber] = useState(1);

  async function getPortfolios(pageNumber: number) {
    const response = await PortfolioApi.getUserPortfolios({
      userId: userId || "", page: pageNumber - 1
    })
    const portfolios = response.elements || [];
    setPortfolios(portfolios);
    setNumberOfPages(response.numberOfPages || 1);
    setLoading(false);
  }

  function nextPage() {
    setPageNumber(pageNumber + 1);
    setLoading(true);
  }

  function prevPage() {
    setPageNumber(pageNumber - 1);
    setLoading(true);
  }

  const portfolioList = portfolios.map(({ id, productIds, userId, name }) => {
    console.log(productIds);
    return <div key={id}>
      <h3>{name}</h3>
      <ProductList portfolioId={id!} products={productIds!} />
    </div>
  })

  React.useEffect(() => {
    setLoading(true);

    getPortfolios(pageNumber);
  }, [pageNumber])

  const body = portfolioList.length > 0 ? loading ? <p>Loading...</p> : portfolioList : <p className='text-center'>No portfolios found.</p>

  return (
    <div>
      <AppNavbar />
      <Container className='justify-content-md-center'>
        {body}
        <Row fluid className='justify-content-center gap-3'>
          <Button className='col-1' disabled={pageNumber == 1} onClick={prevPage}>Poprzednia strona</Button>
          <p className='col-1 text-center'>{pageNumber} ... {numberOfPages}</p>
          <Button className='col-1' disabled={pageNumber == numberOfPages} onClick={nextPage}>Następna strona</Button>
          <Link className='col-1 btn btn-primary' to={`/user/${userId}/portfolios/new`}>Dodaj nowe portfolio</Link>
        </Row>

      </Container>
    </div>
  );
}

export default PortfolioOverview;
