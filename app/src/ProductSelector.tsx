import { PortfolioDTO, ProductControllerApi, ProductDTO, ProductHistoryEntryDTO } from 'NotifffierApi';
import * as React from 'react';
import {ChangeEvent, useEffect, useState} from 'react';
import { Button, Container, FormGroup, Input, Label } from 'reactstrap';
import ApiConfig from './config';

const ProductApi = new ProductControllerApi(ApiConfig);

type SortVariant = "id" | "name"

const ProductSelector: React.FC<{ handleProductChange: (e: React.ChangeEvent<HTMLInputElement>) => void, portfolio: PortfolioDTO }> = ({ handleProductChange: onProductChange, portfolio }) => {
  const [products, setProducts] = useState<ProductDTO[]>([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [sort, setSort] = useState<SortVariant>('name');
  const [totalPages, setTotalPages] = useState(1);
  const [productValues, setProductValues] = useState<Record<string, ProductHistoryEntryDTO | undefined>>({});

  const pageSize = 10; // Number of products to display per page

  useEffect(() => {
    const fetchProducts = async () => {
      const productData = await ProductApi.getAllProducts({ page: currentPage - 1, size: pageSize, sort: [sort] });
      setProducts(productData.elements || []);
      setTotalPages(Math.ceil(productData.numberOfPages || 0 / pageSize)); // Calculate total pages
      const productIds = productData.elements?.map((product) => product.id!) || [];
      fetchProductValues(productIds);
    };

    fetchProducts();
  }, [currentPage, sort]); // Re-fetch products on page change

  const fetchProductValues = async (productIds: string[]) => {
    const values = await ProductApi.getLatestProductValues({ ids: productIds });

    let productValues: Record<string, ProductHistoryEntryDTO> = {};

    for (let entry of values) {
      if (entry.productId != undefined) {
        productValues[entry.productId] = entry;
      }
    }

    setProductValues(productValues);
  };

  const handlePrevPage = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };

  const handleNextPage = () => {
    if (currentPage < totalPages) {
      setCurrentPage(currentPage + 1);
    }
  };

  const handleChangeSort = (event: React.FormEvent<HTMLInputElement>) => {
    switch(event.currentTarget.value) {
      case "id":
        setSort("id");
        break;
      case "name":
        setSort("name");
        break;
    }
  }

  return (
    <div>
      <Label>Products</Label>
      <FormGroup>
        {products.map((product) => {
          const productValue = productValues[product.id!];
          const latestValue = productValue?.close; // Assuming 'close' is the latest value
          return (
            <FormGroup key={product.id} className='d-flex'>
              <p className="col-3">
                {product.name} {latestValue !== undefined && `(${latestValue})`}
              </p>
                <Input
                  id={product.id!.toString()}
                  type="number"
                  value={portfolio.productIds ? portfolio.productIds[product.id!] : 0}
                  checked={portfolio.productIds ? portfolio.productIds[product.id!] !== undefined : false}
                  onChange={onProductChange}
                />
            </FormGroup>
          );
        })}
      </FormGroup>
      <Container className="gap-3 d-flex">
        <Button disabled={currentPage === 1} onClick={handlePrevPage}>
          Previous Page
        </Button>
        <span>Page {currentPage} of {totalPages}</span>
        <Button disabled={currentPage === totalPages} onClick={handleNextPage}>
          Next Page
        </Button>
        <Input type="select" onChange={handleChangeSort} className="container-sm m-0">
          <option>id</option>
          <option>name</option>
        </Input>
      </Container>
    </div>
  );
};

export default ProductSelector;
