import * as React from "react";
import { useState } from "react";
import { useParams } from "react-router-dom";
import { Button, Container, Row } from "reactstrap";
import AppNavbar from "./AppNavbar";
import ProductList from "./ProductList";
import { PortfolioControllerApi, PortfolioDTO } from "NotifffierApi";
import PortfolioForm from "./PortfolioForm";

const PortfolioNew: React.FC<{}> = () => {
  const { userId } = useParams();

  return <div>
      <AppNavbar />
      <Container className='justify-content-md-center'>
        <PortfolioForm userId={userId}/>
      </Container>
    </div>;
}

export default PortfolioNew;
