import * as React from 'react';
import { PortfolioDTO } from 'NotifffierApi';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { PortfolioControllerApi } from 'NotifffierApi';
import { Button, Form, FormGroup, Input, Label, Spinner } from 'reactstrap';
import ProductSelector from './ProductSelector';
import ApiConfig from './config';

const PortfolioApi = new PortfolioControllerApi(ApiConfig);

const PortfolioForm: React.FC<{ portfolioId?: string, userId?: string }> = ({ portfolioId, userId }) => {
  const [portfolio, setPortfolio] = useState<PortfolioDTO>({});
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  if (userId === undefined) {
    userId = portfolio.userId;
  } else {
    portfolio.userId = userId;
  }
  useEffect(() => {
    const fetchPortfolioAndProducts = async () => {
      try {
        setLoading(true);

        if (portfolioId) {
          const portfolioResponse = await PortfolioApi.getPortfolioById({ id: portfolioId });
          setPortfolio(portfolioResponse);
        }

        setLoading(false);
      } catch (e) {
        setError('Failed to fetch portfolio or products');
        setLoading(false);
      }
    };

    fetchPortfolioAndProducts();
  }, [portfolioId]);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setPortfolio({ ...portfolio, [name]: value });
  };

  const handleProductChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { id, value } = e.target;
    const productId = id;
    const amount = parseInt(value);
    const updatedProductIds = { ...portfolio.productIds };

    if (!isNaN(amount) && amount != 0) {
      updatedProductIds[productId] = amount;
    } else {
      delete updatedProductIds[productId];
    }

    setPortfolio({ ...portfolio, productIds: updatedProductIds });
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    try {
      await PortfolioApi.createPortfolio({ portfolioDTO: portfolio });
      alert('Portfolio saved successfully');
      document.location=`/user/${userId}/portfolios`;

    } catch (e) {
      setError('Failed to save portfolio');
    }
  };

  if (loading) {
    return <Spinner />;
  }

  if (error) {
    return <div>{error}</div>;
  }

  return (
    <Form onSubmit={handleSubmit}>
      <FormGroup>
        <Label for="name">Name</Label>
        <Input
          type="text"
          name="name"
          id="name"
          value={portfolio.name || ''}
          onChange={handleChange}
          required
        />
      </FormGroup>
      <FormGroup>
        <Label for="userId">User ID</Label>
        <Input
          name="userId"
          id="userId"
          value={userId || 'sdafsf'}
          onChange={handleChange}
          required
        />
      </FormGroup>
      <FormGroup>
        <ProductSelector handleProductChange={handleProductChange} portfolio={portfolio} />
      </FormGroup>
      <FormGroup className="gap-3 d-flex">
        <Button id="submit" color="primary" type="submit" to={`/user/${userId}/portfolios`}>Save</Button>
        <Button color="secondary" tag={Link} to={`/user/${userId}/portfolios`}>Cancel</Button>
      </FormGroup>
    </Form>
  );
};

export default PortfolioForm;

