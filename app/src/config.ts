import { Configuration } from "NotifffierApi";

const ApiConfig: Configuration = new Configuration({
  basePath: process.env.REACT_APP_BASE_PATH,
})


export default ApiConfig;
