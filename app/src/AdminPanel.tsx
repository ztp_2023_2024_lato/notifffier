import * as React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import * as NotifffierApi from 'NotifffierApi';
import { useState } from 'react';
import { Button, ButtonGroup, Container } from 'reactstrap';
import AppNavbar from './AppNavbar';
import { Link } from 'react-router-dom';
import ApiConfig from './config';

const testApi = new NotifffierApi.TestControllerApi(ApiConfig);

interface PanelButtonInfo {
  name: string;
  amount: number;
  callback: (amount: number) => Promise<void>
}

const Summary: React.FC<{ summary: NotifffierApi.AppSummaryDTO }> = ({ summary }) => {
  return <div>
    <p>Users: {summary.numberOfUsers}</p>
    <p>Products: {summary.numberOfProducts}</p>
    <p>Portfolios: {summary.numberOfPortfolios}</p>
    <p>Alerts: {summary.numberOfAlerts}</p>
    <p>Monitors: {summary.numberOfMonitors}</p>
    <p><Link to={`/user/${summary.userWithMostPortfolios || 1}/portfolios`}>User</Link> with most portfolios: {summary.userWithMostPortfolios} </p>
  </div>;
}

const PanelButton: React.FC<PanelButtonInfo> = ({ name, amount, callback }) => {
  const [loading, setLoading] = useState(false);

  const handleClick = async () => {
    setLoading(true);
    try {
      await callback(amount)
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  }

  return (
    <Button
      className="btn btn-primary mr-2 mb-2"
      onClick={handleClick}
      disabled={loading}
      size="sm"
      key={name}
    >
      Create {amount} {name}
    </Button>
  );
}

const AdminPanel = () => {
  let [loading, setLoading] = useState(true);
  let [summaryDTO, setSummaryDTO] = useState<NotifffierApi.AppSummaryDTO | null>(null);

  async function fetchSummary() {
    setLoading(true);
    const summary = await testApi.summary();
    setSummaryDTO(summary);
    setLoading(false);
  }

  let summary = loading ? <p>Loading summary...</p> : <Summary summary={summaryDTO!} />;

  let buttons: PanelButtonInfo[] = [
    { name: "users", amount: 100, callback: (amount) => testApi.generateUsers({ amount: amount, rounds: 1 }).then(fetchSummary) },
    { name: "products", amount: 50, callback: (amount) => testApi.generateProducts({ amount: amount, rounds: 1 }).then(fetchSummary) },
    { name: "portfolios", amount: 100, callback: (amount) => testApi.generatePortfolios({ amount: amount, rounds: 1 }).then(fetchSummary) },
    { name: "monitors", amount: 5, callback: (amount) => testApi.generateMonitors({ amount: amount, rounds: 1 }).then(fetchSummary) },
    { name: "alerts", amount: 100, callback: (amount) => testApi.generateAlerts({ amount: amount, rounds: 1 }).then(fetchSummary) },
  ];

  React.useEffect(() => {
    fetchSummary()
  }, [])

  return (
    <div>
      <AppNavbar />
      <Container className='justify-content-md-center'>
        <ButtonGroup>
          {buttons.map(PanelButton)}
        </ButtonGroup>
        {summary}
      </Container>
    </div>
  );
};

export default AdminPanel;
