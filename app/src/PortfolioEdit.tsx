import * as React from "react";
import { useParams } from "react-router-dom";
import { Container } from "reactstrap";
import AppNavbar from "./AppNavbar";
import PortfolioForm from "./PortfolioForm";

const PortfolioEdit: React.FC<{}> = () => {
  const {portfolioId} = useParams();

  if (portfolioId === undefined) {
    return <p>Invalid identifier</p>;
  }

  return <div>
      <AppNavbar />
      <Container className='justify-content-md-center'>
        <PortfolioForm portfolioId={portfolioId}/>
      </Container>
    </div>;
}

export default PortfolioEdit;
