import { useState } from 'react';
import { Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import * as React from 'react';

const AppNavbar = () => {

  const [isOpen, setIsOpen] = useState(false);

  return (
    <Navbar color="dark" dark expand="md">
      <NavbarBrand tag={Link} to="/">Notifffier</NavbarBrand>
      <NavbarToggler onClick={() => { setIsOpen(!isOpen) }}/>
      <Collapse isOpen={isOpen} navbar>
        <Nav className="justify-content-end" style={{width: "100%"}} navbar>
          <NavItem>
            <NavLink href="/admin"> <span>&#9881;</span> </NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="https://gitlab.com/luk20001122">Łukasz Niedzielski</NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="https://gitlab.com/madixo">Dominik Madej</NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="https://gitlab.com/ztp_2023_2024_lato/notifffier">GitLab</NavLink>
          </NavItem>
        </Nav>
      </Collapse>
    </Navbar>
  );
};

export default AppNavbar;
