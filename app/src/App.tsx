import * as React from 'react';
import './App.css';
import Home from './Home';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import ProductList from './ProductList';
import PortfolioOverview from './Portfolio';
import AdminPanel from './AdminPanel';
import { Configuration, DefaultConfig } from 'NotifffierApi';
import PortfolioEdit from './PortfolioEdit';
import PortfolioNew from "./PortfolioNew";

const App = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Home/>}/>
        <Route path='/user/:userId/portfolios' element={<PortfolioOverview/>}/>
        <Route path='/portfolio/:portfolioId/edit' element={<PortfolioEdit/>}/>
        <Route path='/user/:userId/portfolios/new' element={<PortfolioNew/>}/>
        <Route path='/admin' element={<AdminPanel/>}/>
      </Routes>
    </Router>
  )
}

export default App;
