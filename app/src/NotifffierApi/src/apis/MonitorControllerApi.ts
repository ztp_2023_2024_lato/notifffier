/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


import * as runtime from '../runtime';
import type {
  CreateMonitorDTO,
  MonitorDTO,
  PageMonitorDTO,
  Pageable,
} from '../models/index';
import {
    CreateMonitorDTOFromJSON,
    CreateMonitorDTOToJSON,
    MonitorDTOFromJSON,
    MonitorDTOToJSON,
    PageMonitorDTOFromJSON,
    PageMonitorDTOToJSON,
    PageableFromJSON,
    PageableToJSON,
} from '../models/index';

export interface CreateMonitorRequest {
    createMonitorDTO: CreateMonitorDTO;
}

export interface DeleteMonitorRequest {
    id: string;
}

export interface GetAllMonitorsRequest {
    pageable: Pageable;
}

export interface GetMonitorByIdRequest {
    id: string;
}

export interface UpdateMonitorRequest {
    id: string;
    monitorDTO: MonitorDTO;
}

/**
 * 
 */
export class MonitorControllerApi extends runtime.BaseAPI {

    /**
     */
    async createMonitorRaw(requestParameters: CreateMonitorRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<MonitorDTO>> {
        if (requestParameters['createMonitorDTO'] == null) {
            throw new runtime.RequiredError(
                'createMonitorDTO',
                'Required parameter "createMonitorDTO" was null or undefined when calling createMonitor().'
            );
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json';

        const response = await this.request({
            path: `/monitor`,
            method: 'POST',
            headers: headerParameters,
            query: queryParameters,
            body: CreateMonitorDTOToJSON(requestParameters['createMonitorDTO']),
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => MonitorDTOFromJSON(jsonValue));
    }

    /**
     */
    async createMonitor(requestParameters: CreateMonitorRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<MonitorDTO> {
        const response = await this.createMonitorRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     */
    async deleteMonitorRaw(requestParameters: DeleteMonitorRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<void>> {
        if (requestParameters['id'] == null) {
            throw new runtime.RequiredError(
                'id',
                'Required parameter "id" was null or undefined when calling deleteMonitor().'
            );
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/monitor/{id}`.replace(`{${"id"}}`, encodeURIComponent(String(requestParameters['id']))),
            method: 'DELETE',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.VoidApiResponse(response);
    }

    /**
     */
    async deleteMonitor(requestParameters: DeleteMonitorRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<void> {
        await this.deleteMonitorRaw(requestParameters, initOverrides);
    }

    /**
     */
    async getAllMonitorsRaw(requestParameters: GetAllMonitorsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<PageMonitorDTO>> {
        if (requestParameters['pageable'] == null) {
            throw new runtime.RequiredError(
                'pageable',
                'Required parameter "pageable" was null or undefined when calling getAllMonitors().'
            );
        }

        const queryParameters: any = {};

        if (requestParameters['pageable'] != null) {
            queryParameters['pageable'] = requestParameters['pageable'];
        }

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/monitor`,
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => PageMonitorDTOFromJSON(jsonValue));
    }

    /**
     */
    async getAllMonitors(requestParameters: GetAllMonitorsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<PageMonitorDTO> {
        const response = await this.getAllMonitorsRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     */
    async getMonitorByIdRaw(requestParameters: GetMonitorByIdRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<MonitorDTO>> {
        if (requestParameters['id'] == null) {
            throw new runtime.RequiredError(
                'id',
                'Required parameter "id" was null or undefined when calling getMonitorById().'
            );
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/monitor/{id}`.replace(`{${"id"}}`, encodeURIComponent(String(requestParameters['id']))),
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => MonitorDTOFromJSON(jsonValue));
    }

    /**
     */
    async getMonitorById(requestParameters: GetMonitorByIdRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<MonitorDTO> {
        const response = await this.getMonitorByIdRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     */
    async updateMonitorRaw(requestParameters: UpdateMonitorRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<MonitorDTO>> {
        if (requestParameters['id'] == null) {
            throw new runtime.RequiredError(
                'id',
                'Required parameter "id" was null or undefined when calling updateMonitor().'
            );
        }

        if (requestParameters['monitorDTO'] == null) {
            throw new runtime.RequiredError(
                'monitorDTO',
                'Required parameter "monitorDTO" was null or undefined when calling updateMonitor().'
            );
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json';

        const response = await this.request({
            path: `/monitor/{id}`.replace(`{${"id"}}`, encodeURIComponent(String(requestParameters['id']))),
            method: 'PUT',
            headers: headerParameters,
            query: queryParameters,
            body: MonitorDTOToJSON(requestParameters['monitorDTO']),
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => MonitorDTOFromJSON(jsonValue));
    }

    /**
     */
    async updateMonitor(requestParameters: UpdateMonitorRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<MonitorDTO> {
        const response = await this.updateMonitorRaw(requestParameters, initOverrides);
        return await response.value();
    }

}
