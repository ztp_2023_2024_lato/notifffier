/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


import * as runtime from '../runtime';
import type {
  AlertDTO,
  AppPageAlertDTO,
  Pageable,
} from '../models/index';
import {
    AlertDTOFromJSON,
    AlertDTOToJSON,
    AppPageAlertDTOFromJSON,
    AppPageAlertDTOToJSON,
    PageableFromJSON,
    PageableToJSON,
} from '../models/index';

export interface DeleteAlertRequest {
    id: string;
}

export interface GetAlertByIdRequest {
    id: string;
}

export interface GetAllAlertsRequest {
    pageable: Pageable;
}

export interface UpdateAlertRequest {
    id: string;
    alertDTO: AlertDTO;
}

/**
 * 
 */
export class AlertControllerApi extends runtime.BaseAPI {

    /**
     */
    async deleteAlertRaw(requestParameters: DeleteAlertRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<void>> {
        if (requestParameters['id'] == null) {
            throw new runtime.RequiredError(
                'id',
                'Required parameter "id" was null or undefined when calling deleteAlert().'
            );
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/alert/{id}`.replace(`{${"id"}}`, encodeURIComponent(String(requestParameters['id']))),
            method: 'DELETE',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.VoidApiResponse(response);
    }

    /**
     */
    async deleteAlert(requestParameters: DeleteAlertRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<void> {
        await this.deleteAlertRaw(requestParameters, initOverrides);
    }

    /**
     */
    async getAlertByIdRaw(requestParameters: GetAlertByIdRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<AlertDTO>> {
        if (requestParameters['id'] == null) {
            throw new runtime.RequiredError(
                'id',
                'Required parameter "id" was null or undefined when calling getAlertById().'
            );
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/alert/{id}`.replace(`{${"id"}}`, encodeURIComponent(String(requestParameters['id']))),
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => AlertDTOFromJSON(jsonValue));
    }

    /**
     */
    async getAlertById(requestParameters: GetAlertByIdRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<AlertDTO> {
        const response = await this.getAlertByIdRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     */
    async getAllAlertsRaw(requestParameters: GetAllAlertsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<AppPageAlertDTO>> {
        if (requestParameters['pageable'] == null) {
            throw new runtime.RequiredError(
                'pageable',
                'Required parameter "pageable" was null or undefined when calling getAllAlerts().'
            );
        }

        const queryParameters: any = {};

        if (requestParameters['pageable'] != null) {
            queryParameters['pageable'] = requestParameters['pageable'];
        }

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/alert`,
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => AppPageAlertDTOFromJSON(jsonValue));
    }

    /**
     */
    async getAllAlerts(requestParameters: GetAllAlertsRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<AppPageAlertDTO> {
        const response = await this.getAllAlertsRaw(requestParameters, initOverrides);
        return await response.value();
    }

    /**
     */
    async updateAlertRaw(requestParameters: UpdateAlertRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<runtime.ApiResponse<AlertDTO>> {
        if (requestParameters['id'] == null) {
            throw new runtime.RequiredError(
                'id',
                'Required parameter "id" was null or undefined when calling updateAlert().'
            );
        }

        if (requestParameters['alertDTO'] == null) {
            throw new runtime.RequiredError(
                'alertDTO',
                'Required parameter "alertDTO" was null or undefined when calling updateAlert().'
            );
        }

        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        headerParameters['Content-Type'] = 'application/json';

        const response = await this.request({
            path: `/alert/{id}`.replace(`{${"id"}}`, encodeURIComponent(String(requestParameters['id']))),
            method: 'PUT',
            headers: headerParameters,
            query: queryParameters,
            body: AlertDTOToJSON(requestParameters['alertDTO']),
        }, initOverrides);

        return new runtime.JSONApiResponse(response, (jsonValue) => AlertDTOFromJSON(jsonValue));
    }

    /**
     */
    async updateAlert(requestParameters: UpdateAlertRequest, initOverrides?: RequestInit | runtime.InitOverrideFunction): Promise<AlertDTO> {
        const response = await this.updateAlertRaw(requestParameters, initOverrides);
        return await response.value();
    }

}
