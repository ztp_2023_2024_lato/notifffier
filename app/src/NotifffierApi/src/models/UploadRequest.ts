/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface UploadRequest
 */
export interface UploadRequest {
    /**
     * 
     * @type {Blob}
     * @memberof UploadRequest
     */
    file: Blob;
}

/**
 * Check if a given object implements the UploadRequest interface.
 */
export function instanceOfUploadRequest(value: object): boolean {
    if (!('file' in value)) return false;
    return true;
}

export function UploadRequestFromJSON(json: any): UploadRequest {
    return UploadRequestFromJSONTyped(json, false);
}

export function UploadRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): UploadRequest {
    if (json == null) {
        return json;
    }
    return {
        
        'file': json['file'],
    };
}

export function UploadRequestToJSON(value?: UploadRequest | null): any {
    if (value == null) {
        return value;
    }
    return {
        
        'file': value['file'],
    };
}

