/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface AppUserDTO
 */
export interface AppUserDTO {
    /**
     * 
     * @type {string}
     * @memberof AppUserDTO
     */
    id?: string;
    /**
     * 
     * @type {string}
     * @memberof AppUserDTO
     */
    name?: string;
    /**
     * 
     * @type {string}
     * @memberof AppUserDTO
     */
    surname?: string;
    /**
     * 
     * @type {string}
     * @memberof AppUserDTO
     */
    username?: string;
}

/**
 * Check if a given object implements the AppUserDTO interface.
 */
export function instanceOfAppUserDTO(value: object): boolean {
    return true;
}

export function AppUserDTOFromJSON(json: any): AppUserDTO {
    return AppUserDTOFromJSONTyped(json, false);
}

export function AppUserDTOFromJSONTyped(json: any, ignoreDiscriminator: boolean): AppUserDTO {
    if (json == null) {
        return json;
    }
    return {
        
        'id': json['id'] == null ? undefined : json['id'],
        'name': json['name'] == null ? undefined : json['name'],
        'surname': json['surname'] == null ? undefined : json['surname'],
        'username': json['username'] == null ? undefined : json['username'],
    };
}

export function AppUserDTOToJSON(value?: AppUserDTO | null): any {
    if (value == null) {
        return value;
    }
    return {
        
        'id': value['id'],
        'name': value['name'],
        'surname': value['surname'],
        'username': value['username'],
    };
}

