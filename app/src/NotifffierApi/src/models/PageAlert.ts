/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { mapValues } from '../runtime';
import type { Alert } from './Alert';
import {
    AlertFromJSON,
    AlertFromJSONTyped,
    AlertToJSON,
} from './Alert';
import type { PageableObject } from './PageableObject';
import {
    PageableObjectFromJSON,
    PageableObjectFromJSONTyped,
    PageableObjectToJSON,
} from './PageableObject';
import type { SortObject } from './SortObject';
import {
    SortObjectFromJSON,
    SortObjectFromJSONTyped,
    SortObjectToJSON,
} from './SortObject';

/**
 * 
 * @export
 * @interface PageAlert
 */
export interface PageAlert {
    /**
     * 
     * @type {number}
     * @memberof PageAlert
     */
    totalPages?: number;
    /**
     * 
     * @type {number}
     * @memberof PageAlert
     */
    totalElements?: number;
    /**
     * 
     * @type {PageableObject}
     * @memberof PageAlert
     */
    pageable?: PageableObject;
    /**
     * 
     * @type {number}
     * @memberof PageAlert
     */
    size?: number;
    /**
     * 
     * @type {Array<Alert>}
     * @memberof PageAlert
     */
    content?: Array<Alert>;
    /**
     * 
     * @type {number}
     * @memberof PageAlert
     */
    number?: number;
    /**
     * 
     * @type {Array<SortObject>}
     * @memberof PageAlert
     */
    sort?: Array<SortObject>;
    /**
     * 
     * @type {boolean}
     * @memberof PageAlert
     */
    last?: boolean;
    /**
     * 
     * @type {boolean}
     * @memberof PageAlert
     */
    first?: boolean;
    /**
     * 
     * @type {number}
     * @memberof PageAlert
     */
    numberOfElements?: number;
    /**
     * 
     * @type {boolean}
     * @memberof PageAlert
     */
    empty?: boolean;
}

/**
 * Check if a given object implements the PageAlert interface.
 */
export function instanceOfPageAlert(value: object): boolean {
    return true;
}

export function PageAlertFromJSON(json: any): PageAlert {
    return PageAlertFromJSONTyped(json, false);
}

export function PageAlertFromJSONTyped(json: any, ignoreDiscriminator: boolean): PageAlert {
    if (json == null) {
        return json;
    }
    return {
        
        'totalPages': json['totalPages'] == null ? undefined : json['totalPages'],
        'totalElements': json['totalElements'] == null ? undefined : json['totalElements'],
        'pageable': json['pageable'] == null ? undefined : PageableObjectFromJSON(json['pageable']),
        'size': json['size'] == null ? undefined : json['size'],
        'content': json['content'] == null ? undefined : ((json['content'] as Array<any>).map(AlertFromJSON)),
        'number': json['number'] == null ? undefined : json['number'],
        'sort': json['sort'] == null ? undefined : ((json['sort'] as Array<any>).map(SortObjectFromJSON)),
        'last': json['last'] == null ? undefined : json['last'],
        'first': json['first'] == null ? undefined : json['first'],
        'numberOfElements': json['numberOfElements'] == null ? undefined : json['numberOfElements'],
        'empty': json['empty'] == null ? undefined : json['empty'],
    };
}

export function PageAlertToJSON(value?: PageAlert | null): any {
    if (value == null) {
        return value;
    }
    return {
        
        'totalPages': value['totalPages'],
        'totalElements': value['totalElements'],
        'pageable': PageableObjectToJSON(value['pageable']),
        'size': value['size'],
        'content': value['content'] == null ? undefined : ((value['content'] as Array<any>).map(AlertToJSON)),
        'number': value['number'],
        'sort': value['sort'] == null ? undefined : ((value['sort'] as Array<any>).map(SortObjectToJSON)),
        'last': value['last'],
        'first': value['first'],
        'numberOfElements': value['numberOfElements'],
        'empty': value['empty'],
    };
}

