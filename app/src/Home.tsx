import * as React from 'react';
import './App.css';
import AppNavbar from './AppNavbar';
import { Link } from 'react-router-dom';
import { Button, Container } from 'reactstrap';

const Home = () => {
  return (
    <div>
      <AppNavbar/>
      <Container className="justify-content-md-center">
        <Button color="link"><Link to="/user/1/portfolios">my portfolios</Link></Button>
      </Container>
    </div>
  );
}

export default Home;
