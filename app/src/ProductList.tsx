import { useState, useEffect } from 'react';
import { Button, Container, Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import * as React from 'react';
import { ProductControllerApi } from 'NotifffierApi';
import ApiConfig from './config';

const ProductApi = new ProductControllerApi(ApiConfig);

interface PortfolioProductList {
  portfolioId: string,
  products: { [key: string]: number; }
}

const ProductList: React.FC<PortfolioProductList> = ({ portfolioId, products }) => {

  const [productPrices, setProductPrices] = useState<{ [key: string]: number }>({});

  const groupList = Object.entries(products).map(([productId, quantity]) => {
    const price = productPrices[productId];

    const value = quantity * productPrices[productId] | NaN;

    return <tr key={productId}>
      <td>{productId}</td>
      <td>{quantity}</td>
      <td>{price}</td>
      <td>{value}</td>
    </tr>
  });

  useEffect(() => {
    async function getPrices() {
      const productIds = Object.keys(products);
      const productData = await ProductApi.getLatestProductValues({ids: productIds});
      const productClosePrices = 
productData.map(entry => {
        return [entry.productId, entry.close || NaN];
      });
      const productPrices = Object.fromEntries(productClosePrices);
      setProductPrices(productPrices);
    }

    getPrices();
  }, [])

  return (
    <Container fluid>
      <div className="float-end">
        <Button color="success" tag={Link} to={`/portfolio/${portfolioId}/edit`}>Edit products</Button>
      </div>
      <Table className="mt-4 col-sm">
        <thead>
          <tr>
            <th className="col-1">Product ID</th>
            <th className="col-3">amount</th>
            <th className="col-3">price</th>
            <th className="col-3">total value</th>
          </tr>
        </thead>
        <tbody>
          {groupList}
        </tbody>
      </Table>
    </Container>
  );
};

export default ProductList;
