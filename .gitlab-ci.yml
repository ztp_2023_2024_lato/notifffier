stages:
  - 'lint'
  - 'build'
  - 'test'
  - 'docker_build'
  - 'e2e'
  - 'deploy'

variables:
  IMAGE_NAME: '$CI_COMMIT_SHORT_SHA'
  IMAGE_NAME_LATEST: 'latest'
  APP: 'app'
  WEB: 'web'

default:
  artifacts:
    when: 'on_success'
    expire_in: '30 days'
    access: 'developer'
  cache:
    policy: pull-push
    unprotect: false
    untracked: false
    when: on_success

.java:
  image: 'eclipse-temurin:22-jdk-alpine'
  variables:
    M2_LOCATION: '$CI_PROJECT_DIR/.m2/repository'
    MAVEN_OPTS: '-Dmaven.repo.local=$M2_LOCATION'
  cache:
    key:
      files:
        - 'pom.xml'
    paths:
      - '$M2_LOCATION'

.node:
  image: 'node:20.14.0-alpine3.20'
  variables:
    npm_config_cache: '.npm'
    npm_config_prefer_offline: true

lint_app:
  stage: 'lint'
  extends: '.java'
  dependencies: []
  script:
    - './mvnw checkstyle:checkstyle spotbugs:spotbugs'
  artifacts:
    name: 'lint-$CI_COMMIT_SHORT_SHA'
    paths:
      - 'target/spotbugsXml.xml'
      - 'target/spotbugs.html'
      - 'target/site/'
      - 'target/checkstyle-result.xml'

lint_web:
  stage: 'lint'
  extends: '.node'
  dependencies: []
  variables:
    ESLINT_OUTPUT: 'target/eslint.html'
  cache:
    key: '$CI_JOB_NAME'
    paths:
      - '.npm'
  script:
    - 'npm install -D eslint @eslint/js typescript typescript-eslint eslint-config-react-app'
    - 'npx eslint app -o $ESLINT_OUTPUT -f html || true'
  artifacts:
    name: 'lint-$CI_COMMIT_SHORT_SHA'
    paths:
      - '$ESLINT_OUTPUT'

build_app:
  stage: 'build'
  extends: '.java'
  dependencies: []
  script:
    - './mvnw package -DskipTests'
  artifacts:
    name: 'build-$CI_COMMIT_SHORT_SHA'
    paths:
      - 'target'

build_web:
  stage: 'build'
  extends: '.node'
  dependencies: []
  cache:
    key:
      files:
        - 'app/package-lock.json'
    paths:
      - 'app/.npm'
  script:
    - 'cd app'
    - 'npm ci'
    - 'unset CI'
    - 'npm run build:prod'
  artifacts:
    name: 'build-$CI_COMMIT_SHORT_SHA'
    paths:
      - 'app/build'

test_app:
  stage: 'test'
  extends: '.java'
  dependencies:
    - 'build_app'
  script:
    - './mvnw surefire:test'

.docker:
  image: 'docker:latest'
  variables:
    DOCKER_HOST: 'tcp://docker:2375'
  services:
    - name: 'docker:dind'
      variables:
        DOCKER_TLS_CERTDIR: ''

docker_build:
  stage: 'docker_build'
  extends: '.docker'
  dependencies: []
  script:
    - 'docker login $CI_REGISTRY -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD'
    - 'docker build -t $CI_REGISTRY_IMAGE/$APP:$IMAGE_NAME -t $CI_REGISTRY_IMAGE/$APP:$IMAGE_NAME_LATEST -f docker/$APP/dockerfile .'
    - 'docker build -t $CI_REGISTRY_IMAGE/$WEB:$IMAGE_NAME -t $CI_REGISTRY_IMAGE/$WEB:$IMAGE_NAME_LATEST -f docker/$WEB/dockerfile .'
    - 'docker push -a $CI_REGISTRY_IMAGE/$APP'
    - 'docker push -a $CI_REGISTRY_IMAGE/$WEB'
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'

e2e:
  stage: "e2e"
  extends: '.docker'
  dependencies:
    - 'docker_build'
  needs:
    - 'docker_build'
  script:
    - 'mkdir -p e2e/screenshots e2e/log/app e2e/log/nginx certs'
    - 'cp $CERT_CRT certs/cert.crt'
    - 'cp $CERT_KEY certs/cert.key'
    - 'docker login $CI_REGISTRY -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD'
    - 'docker compose -f compose.yaml -f e2e/compose.yaml run cypress'
  artifacts:
    name: 'screenshots'
    when: 'on_failure'
    expire_in: '30 days'
    access: 'developer'
    paths:
      - 'e2e/log'
      - 'e2e/screenshots'
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'

deploy:
  stage: 'deploy'
  image: 'alpine:latest'
  dependencies:
    - 'docker_build'
  needs:
    - 'docker_build'
  variables:
    URL: '$DEPLOY_USERNAME@$DEPLOY_IP'
    COMPOSE_NAME: 'compose.yaml'
    COMPOSE_DEPLOY_NAME: 'compose-deploy.yaml'
  script:
    - 'apk update && apk add openssh yq'
    - 'yq "del(.services.$APP.build) | .services.$APP.image = \"$CI_REGISTRY_IMAGE/$APP:$IMAGE_NAME\"" $COMPOSE_NAME -i'
    - 'yq "del(.services.$WEB.build) | .services.$WEB.image = \"$CI_REGISTRY_IMAGE/$WEB:$IMAGE_NAME\"" $COMPOSE_NAME -i'
    - 'chmod 0600 $SSH_IDENTITY'
    - 'scp -o StrictHostKeyChecking=no -i $SSH_IDENTITY $COMPOSE_NAME $URL:$COMPOSE_DEPLOY_NAME'
    - 'ssh -o StrictHostKeyChecking=no -i $SSH_IDENTITY $URL "docker compose down --rmi all; mv $COMPOSE_DEPLOY_NAME $COMPOSE_NAME; docker compose up -d"'
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'
  when: 'manual'