# Badanie wydajności

Celem tej pracy jest porównanie wydajności silników bazodanowych w kontekście aplikacji webowej,
wybrane do porównania silniki to: Postgres, Oracle database, MongoDB oraz Cassandra.
W ramach projektu przygotowano:
 - aplikacja Notifffier - aplikacja webowa wykorzystująca framework spring boot,
 - `TestController.java` - umożliwia generowanie danych testowych (endpointy `generate...`), 
   oraz usuwanie wierszy z poszczególnych tabel (endpoint `purge`)
 - repozytoria `CrudRepository` - operacje tworzenia, odczytu, aktualizacji oraz usuwania encji
 - repozytoria `PagingRepository` - funkcje do pobierania encji z podziałem na strony, z możliwością sortowania po kolumnie
 - dodatkowe zapytania - wyszukiwanie użytkownika po nazwie, pobieranie portfolio
   posortowanych po wartości, pobieranie użytkownika z nawiększą portfolio
 - konfiguracja swaggera - umożliwia łatwe wołanie endpointów
 - konfiguracja glowroot - umożliwia łatwy odczyt średnich czasów wykonania poszczególnych zapytań w wywołanym endpoincie

## Aplikacja Notifffier

TODO


## Swagger

![img_3.png](img_3.png)
![img_2.png](img_2.png)

## Glowroot

![img.png](img.png)

![img_1.png](img_1.png)

## Pomiary

### PostgreSQL oraz Oracle
W przypadku baz relacyjnych biblioteka hibernate korzysta z interfejsu JDBC, 
narzędzie glowroot zbiera informacje o czasach wykonania zapytań oraz wyświetla je w interfejsie w przeglądarce.

### MongoDB

### Cassandra

## Procedura testowa
W celu zbadania wydajności wymyślono 2 kategorie testów:
- operacje crud:
  - operacje tworzenia encji,
  - operacje odczytu encji,
  - operacje zmiany encji,
  - operacje usuwania encji,
- zapytania dodatkowe:
  - zapytania wykorzystujące funkcje agregujące, grupowanie

### Grupowe wstawianie
Framework Spring umożliwia grupowe operacje na encjach, 

### CRUD
Miarą wydajności jest średni czas wykonania zapytania w ramach transakcji,
w zależności od ilości istniejących danych.
Operacje wykonano dla rozmiarów istniejących danych: 0, 1000, 100000, 1000000.
Średnia wyznaczana została na podstawie wielokrotnego wykonania transakcji,
ilości próbek zostały podane w opisie każdej kategorii

#### Create
Operacje tworzenia przetestowano poprzez wywołanie http://localhost:8080/debug/generateUsers?amount=1&rounds=1000.

