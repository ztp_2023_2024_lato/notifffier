package com.example.notifffier.service;

import com.example.notifffier.NotifffierApplication;
import com.example.notifffier.api.DashboardApiTest;
import com.example.notifffier.domain.*;
import com.example.notifffier.domain.monitor.Monitor;
import com.example.notifffier.domain.monitor.PortfolioPriceMonitor;
import jakarta.annotation.PostConstruct;
import jakarta.persistence.EntityManagerFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Map;

@SpringBootTest(classes = NotifffierApplication.class)
@AutoConfigureMockMvc
@ContextConfiguration(classes = DashboardApiTest.TestConfig.class)
public class ServiceTest {
    @Autowired
    private ProductService productService;

    @Autowired
    private AlertService alertService;

    @Autowired
    private PortfolioService portfolioService;

    @Autowired
    private AppUserService userService;

    @Autowired
    private EntityManagerFactory factory;

    @PostConstruct
    public void testEntityManagerFactory() {
        Session manager = factory.unwrap(SessionFactory.class).openSession();

        Transaction t = manager.beginTransaction();

        AppUser user = new AppUser();
        manager.persist(user);
        user.setName("lukasz");
        user.setSurname("niedzielski");
        user.setUsername("lukasz_123");
        user.setPasswordHash("lukasz_123");

        Monitor monitor = new PortfolioPriceMonitor();
        manager.persist(monitor);
        monitor.setDefinedBy(user);
        monitor.setCron("* * * * * *");
        monitor.setName("testowa definicja");

        Alert alert = new Alert();
        manager.persist(alert);
        alert.setMessage("testowy alert");
        alert.setAppUser(user);
        alert.setMonitor(monitor);

        Product product = new Product();
        manager.persist(product);
        product.setCategory("GPW");
        product.setFullName("WIG20 FUTURE");
        product.setName("WIG20 FUTURE");
        product.setSymbol("FW20");

        ProductHistoryEntry entry = new ProductHistoryEntry();
        manager.persist(entry);
        entry.setProduct(product);
        entry.setTimestamp(LocalDateTime.of(1998, Month.JANUARY,20, 0, 0));
        entry.setOpen(1433);
        entry.setHigh(1485);
        entry.setLow(1433);
        entry.setClose(1485);
        entry.setVolume(22);
        entry.setOpenInterest(25);

        Portfolio portfolio = new Portfolio();
        manager.persist(portfolio);
        portfolio.setName("testowe portfolio");
        portfolio.setProducts(Map.of(product, BigDecimal.TEN));
        portfolio.setUser(user);

        manager.flush();

        t.commit();
    }

    @Test
    public void testServices() {
        assert productService.findAll().iterator().hasNext() : "Expected some value";
        assert alertService.findAll().iterator().hasNext() : "Expected some value";
        assert portfolioService.findAll().iterator().hasNext() : "Expected some value";
        assert userService.findAll().iterator().hasNext() : "Expected some value";
    }
}
