package com.example.notifffier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestNotifffierApplication {
    public static void main(String[] args) {
        SpringApplication.run(NotifffierApplication.class, args);
    }
}
