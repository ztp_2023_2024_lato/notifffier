package com.example.notifffier.api;

import com.example.notifffier.NotifffierApplication;
import com.example.notifffier.repository.AlertRepository;
import com.example.notifffier.util.Symbol;
import com.example.notifffier.util.SymbolDataFetcher;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = NotifffierApplication.class)
@AutoConfigureMockMvc
@ContextConfiguration(classes = DashboardApiTest.TestConfig.class)
public class DashboardApiTest {

    @Autowired
    MockMvc mvc;

    @Test
    void importTest() throws Exception {
        MockMultipartFile file
                = new MockMultipartFile(
                "file",
                "test.txt",
                MediaType.TEXT_PLAIN_VALUE,
                """
                        <TICKER>,<PER>,<DATE>,<TIME>,<OPEN>,<HIGH>,<LOW>,<CLOSE>,<VOL>,<OPENINT>
                        FW20,D,19980120,000000,1440,1440,1440,1440,4,12
                        FW20,D,19980122,000000,1370,1370,1370,1370,9,21
                        FW20,D,19980123,000000,1370,1370,1370,1370,2,22
                        FW20,D,19980128,000000,1382,1390,1382,1389,8,9
                        FW20,D,19980129,000000,1417,1417,1412,1412,4,21
                        FW20,D,19980130,000000,1410,1410,1380,1380,2,9
                        FW20,D,19980202,000000,1433,1485,1433,1485,22,25
                        """.getBytes()
        );
        mvc.perform(multipart("/dashboard/products/import")
                        .file(file))
                .andExpect(status().is(200));
    }

    @Configuration
    public static class TestConfig {
        SymbolDataFetcher symbolDataFetcher = Mockito.mock(SymbolDataFetcher.class);
        Symbol symbol = new Symbol("FW20", "WIG20 FUTURE", "WIG20 FUTURE", "GPW");

        @Bean
        public SymbolDataFetcher symbolDataFetcher() throws IOException {
            when(symbolDataFetcher.fetchAndParseSymbolData(any())).thenReturn(List.of());
            when(symbolDataFetcher.fetchAndParseSymbolData("FW20")).thenReturn(List.of(symbol));

            return symbolDataFetcher;
        }
    }
}
