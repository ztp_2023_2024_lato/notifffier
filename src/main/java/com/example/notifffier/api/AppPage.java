package com.example.notifffier.api;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.List;


@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class AppPage<T> {
    public final int page;
    public final int numberOfPages;
    public final List<T> elements;

    public static <T> AppPage<T> from(Page<T> page) {
        return new AppPage<T>(
                page.getNumber(),
                page.getTotalPages(),
                page.getContent()
        );
    }
}
