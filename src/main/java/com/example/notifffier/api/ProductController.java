package com.example.notifffier.api;

import com.example.notifffier.api.dto.ProductDTO;
import com.example.notifffier.api.dto.ProductHistoryEntryDTO;
import com.example.notifffier.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@CrossOrigin
@RestController
@EnableSpringDataWebSupport
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;

    @GetMapping
    public AppPage<ProductDTO> getAllProducts(Integer page, Integer size, String[] sort) {
        Pageable pageable = PageRequest.of(page, size, Sort.by(sort));
        return AppPage.from(productService.getAllProducts(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductDTO> getProductById(@PathVariable UUID id) {
        Optional<ProductDTO> product = productService.getProductById(id);
        return product.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProductDTO> updateProduct(@PathVariable UUID id, @RequestBody ProductDTO productDTO) {
        Optional<ProductDTO> updatedProduct = productService.updateProduct(id, productDTO);
        return updatedProduct.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable UUID id) {
        productService.deleteProduct(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/latest-value")
    public ResponseEntity<List<ProductHistoryEntryDTO>> getLatestProductValues(@RequestParam List<UUID> ids) {
        List<ProductHistoryEntryDTO> latestValues = productService.getLatestProductHistories(ids);
        return ResponseEntity.ok(latestValues);
    }
}
