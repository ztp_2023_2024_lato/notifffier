package com.example.notifffier.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Map;
import java.util.UUID;

@Getter
@Setter
public class PortfolioDTO {
    private UUID id;
    private Map<UUID, BigDecimal> productIds;
    private UUID userId;
    private String name;
}
