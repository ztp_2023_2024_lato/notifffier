package com.example.notifffier.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
public class ProductHistoryEntryDTO {
    private UUID id;
    private UUID productId;
    private LocalDateTime date;
    private double open;
    private double high;
    private double low;
    private double close;
    private double volume;
    private double openInterest;
}
