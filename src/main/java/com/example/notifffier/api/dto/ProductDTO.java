package com.example.notifffier.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class ProductDTO {
    private UUID id;
    private String symbol;
    private String name;
    private String fullName;
    private String category;
}