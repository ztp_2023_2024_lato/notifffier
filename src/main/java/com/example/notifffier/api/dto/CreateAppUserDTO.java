package com.example.notifffier.api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateAppUserDTO {
    private String name;
    private String surname;
    private String username;
    private String password;
}
