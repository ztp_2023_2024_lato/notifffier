package com.example.notifffier.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class AppUserDTO {
    private UUID id;
    private String name;
    private String surname;
    private String username;
}

