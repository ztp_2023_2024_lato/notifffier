package com.example.notifffier.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class AppSummaryDTO {
    private Long numberOfAlerts;
    private Long numberOfUsers;
    private Long numberOfMonitors;
    private Long numberOfProducts;
    private Long numberOfPortfolios;
    private UUID userWithMostPortfolios;
}
