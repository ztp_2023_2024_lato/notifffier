package com.example.notifffier.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class MonitorDTO {
    private UUID id;
    private String name;
    private String cron;
    private UUID definedById;
}

