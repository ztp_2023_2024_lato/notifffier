package com.example.notifffier.api.dto;


import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class AlertDTO {
    private UUID id;
    private UUID appUserId;
    private UUID monitorId;
    private String message;
    private UUID productId;
    private UUID portfolioId;
}
