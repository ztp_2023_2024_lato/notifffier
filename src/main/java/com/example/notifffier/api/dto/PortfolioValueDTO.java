package com.example.notifffier.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.UUID;

@AllArgsConstructor
@Getter
@Setter
public class PortfolioValueDTO {
    UUID portfolioId;
    BigDecimal value;
}
