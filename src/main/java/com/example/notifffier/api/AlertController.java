package com.example.notifffier.api;

import com.example.notifffier.api.dto.AlertDTO;
import com.example.notifffier.service.AlertService;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@CrossOrigin
@RestController
@EnableSpringDataWebSupport
@RequiredArgsConstructor
@RequestMapping("/alert")
public class AlertController {

    @PostConstruct
    public void init() {
        System.out.println(this.getClass().getName());
    }

    private final AlertService alertService;

    @GetMapping
    public AppPage<AlertDTO> getAllAlerts(Pageable pageable) {
        return AppPage.from(alertService.getAllAlerts(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<AlertDTO> getAlertById(@PathVariable UUID id) {
        Optional<AlertDTO> alert = alertService.getAlertById(id);
        return alert.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<AlertDTO> updateAlert(@PathVariable UUID id, @RequestBody AlertDTO alertDTO) {
        Optional<AlertDTO> updatedAlert = alertService.updateAlert(id, alertDTO);
        return updatedAlert.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAlert(@PathVariable UUID id) {
        alertService.deleteAlert(id);
        return ResponseEntity.noContent().build();
    }
}
