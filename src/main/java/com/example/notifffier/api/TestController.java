package com.example.notifffier.api;

import com.example.notifffier.api.dto.AppSummaryDTO;
import com.example.notifffier.dummydata.*;
import com.example.notifffier.repository.*;
import jakarta.annotation.PostConstruct;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin
@RestController
@EnableSpringDataWebSupport
@RequiredArgsConstructor
@RequestMapping("/debug")
public class TestController {

    private final UserInserter userInserter;
    private final ProductInserter productInserter;
    private final PortfolioGenerator portfolioGenerator;
    private final MonitorGenerator monitorGenerator;
    private final AlertGenerator alertGenerator;
    private final AlertRepository alertRepository;
    private final MonitorRepository monitorRepository;
    private final ProductHistoryRepository productHistoryRepository;
    private final PortfolioRepository portfolioRepository;
    private final ProductRepository productRepository;
    private final AppUserRepository appUserRepository;

    private final EntityManager entityManager;

    public enum EntityType {
        AppUser, Product, Portfolio, Alert, Monitor, ProductHistory
    }

    @GetMapping("/test-request")
    public ResponseEntity<String> testPostRequest() {
        return ResponseEntity.ok("GET request successful");
    }

    @PostConstruct
    public void init() {
        System.out.println(this.getClass().getName());
    }

    @PostMapping("/generateUsers")
    public ResponseEntity<Void> generateUsers(@RequestParam int amount, @RequestParam int rounds) {
        for (int i = 0; i < rounds; i++) {
            userInserter.generate(amount);
        }
        return ResponseEntity.ok(null);
    }

    @PostMapping("/generateProducts")
    public ResponseEntity<Void> generateProducts(@RequestParam int amount, @RequestParam int rounds) {
        for (int i = 0; i < rounds; i++) {
            productInserter.generate(amount);
        }
        return ResponseEntity.ok(null);
    }

    @PostMapping("/generatePortfolios")
    public ResponseEntity<Void> generatePortfolios(@RequestParam int amount, @RequestParam int rounds) {
        for (int i = 0; i < rounds; i++) {
            portfolioGenerator.generate(amount);
        }
        return ResponseEntity.ok(null);
    }

    @PostMapping("/generateAlerts")
    public ResponseEntity<Void> generateAlerts(@RequestParam int amount, @RequestParam int rounds) {
        for (int i = 0; i < rounds; i++) {
            alertGenerator.generate(amount);
        }
        return ResponseEntity.ok(null);
    }

    @PostMapping("/generateMonitors")
    public ResponseEntity<Void> generateMonitors(@RequestParam int amount, @RequestParam int rounds) {
        for (int i = 0; i < rounds; i++) {
            monitorGenerator.generate(amount);
        }
        return ResponseEntity.ok(null);
    }

    @PostMapping("/purge")
    @Transactional
    public ResponseEntity<Void> purge(Optional<EntityType> type) {
        if (type.isEmpty()) {
            entityManager.createNativeQuery("TRUNCATE TABLE ALERT CASCADE").executeUpdate();
            entityManager.createNativeQuery("TRUNCATE TABLE MONITOR CASCADE").executeUpdate();
            entityManager.createNativeQuery("TRUNCATE TABLE PRODUCT_HISTORY_ENTRY CASCADE").executeUpdate();
            entityManager.createNativeQuery("TRUNCATE TABLE PORTFOLIO CASCADE").executeUpdate();
            entityManager.createNativeQuery("TRUNCATE TABLE PRODUCT CASCADE").executeUpdate();
            entityManager.createNativeQuery("TRUNCATE TABLE APP_USER CASCADE").executeUpdate();
            return ResponseEntity.ok(null);
        }
        switch (type.get()) {
            case AppUser -> {
                entityManager.createNativeQuery("TRUNCATE TABLE APP_USER").executeUpdate();
            }
            case Product -> {
                entityManager.createNativeQuery("TRUNCATE TABLE PRODUCT").executeUpdate();
            }
            case Portfolio -> {
                entityManager.createNativeQuery("TRUNCATE TABLE PORTFOLIO").executeUpdate();
            }
            case Alert -> {
                entityManager.createNativeQuery("TRUNCATE TABLE ALERT").executeUpdate();
            }
            case Monitor -> {
                entityManager.createNativeQuery("TRUNCATE TABLE MONITOR").executeUpdate();
            }
            case ProductHistory -> {
                entityManager.createNativeQuery("TRUNCATE TABLE PRODUCT_HISTORY_ENTRY").executeUpdate();
            }
        }

        return ResponseEntity.ok(null);
    }

    @GetMapping("/summary")
    public ResponseEntity<AppSummaryDTO> summary() {
        AppSummaryDTO summaryDTO = new AppSummaryDTO();
        summaryDTO.setNumberOfAlerts(alertRepository.count());
        summaryDTO.setNumberOfMonitors(monitorRepository.count());
        summaryDTO.setNumberOfUsers(appUserRepository.count());
        summaryDTO.setNumberOfPortfolios(portfolioRepository.count());
        summaryDTO.setNumberOfProducts(productRepository.count());
        summaryDTO.setUserWithMostPortfolios(portfolioRepository.findUserWithMostPortfolios().orElse(null));

        return ResponseEntity.ok(summaryDTO);
    }
}
