package com.example.notifffier.api;

import com.example.notifffier.domain.Alert;
import com.example.notifffier.domain.monitor.Monitor;
import com.example.notifffier.service.AlertService;
import com.example.notifffier.service.MonitorService;
import com.example.notifffier.service.ProductCsvReader;
import com.example.notifffier.service.ProductService;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@CrossOrigin
@RestController
@EnableSpringDataWebSupport
@RequiredArgsConstructor
@RequestMapping("/dashboard")
public class DashboardController {
    private final ProductService productService;
    private final AlertService alertService;
    private final MonitorService monitorService;

    @PostConstruct
    public void init() {
        System.out.println(this.getClass().getName());
    }

    @GetMapping("/notifications")
    public ResponseEntity<AppPage<Alert>> getUserNotifications(@ParameterObject final Pageable pageable) {
        return ResponseEntity.ok(
                AppPage.from(alertService.findAll(pageable))
        );
    }

    @GetMapping("/monitors")
    public ResponseEntity<AppPage<Monitor>> getUserMonitors(final Pageable pageable) {
        return ResponseEntity.ok(
                AppPage.from(monitorService.findAll(pageable))
        );
    }

    @GetMapping("/summary")
    public ResponseEntity<Object> summary() {
        return null;
    }

    @PostMapping("/products/import")
    public ResponseEntity<?> upload(@RequestParam("file") MultipartFile file) {
        try {
            ProductCsvReader.read(file, productService);
            return ResponseEntity.ok(null);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
