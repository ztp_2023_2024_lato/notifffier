package com.example.notifffier.api;

import com.example.notifffier.api.dto.CreateMonitorDTO;
import com.example.notifffier.api.dto.MonitorDTO;
import com.example.notifffier.domain.monitor.Monitor;
import com.example.notifffier.service.MonitorService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@CrossOrigin
@RestController
@EnableSpringDataWebSupport
@RequiredArgsConstructor
@RequestMapping("/monitor")
public class MonitorController {
    private final MonitorService monitorService;

    @GetMapping
    public Page<MonitorDTO> getAllMonitors(Pageable pageable) {
        return monitorService.getAllMonitors(pageable);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MonitorDTO> getMonitorById(@PathVariable UUID id) {
        Optional<MonitorDTO> monitor = monitorService.getMonitorById(id);
        return monitor.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public MonitorDTO createMonitor(@RequestBody CreateMonitorDTO createMonitorDTO) {
        return monitorService.createMonitor(createMonitorDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<MonitorDTO> updateMonitor(@PathVariable UUID id, @RequestBody MonitorDTO monitorDTO) {
        Optional<MonitorDTO> updatedMonitor = monitorService.updateMonitor(id, monitorDTO);
        return updatedMonitor.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteMonitor(@PathVariable UUID id) {
        monitorService.deleteMonitor(id);
        return ResponseEntity.noContent().build();
    }
}
