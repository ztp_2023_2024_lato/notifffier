package com.example.notifffier.api;

import com.example.notifffier.api.dto.AppUserDTO;
import com.example.notifffier.api.dto.CreateAppUserDTO;
import com.example.notifffier.domain.Alert;
import com.example.notifffier.domain.AppUser;
import com.example.notifffier.repository.AlertRepository;
import com.example.notifffier.service.AlertService;
import com.example.notifffier.service.AppUserService;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.http.ResponseEntity.notFound;

@CrossOrigin
@RestController
@EnableSpringDataWebSupport
@RequiredArgsConstructor
@RequestMapping("/user")
public class AppUserController {
    private final AppUserService appUserService;
    private final AlertRepository alertRepository;

    @PostConstruct
    public void init() {
        System.out.println(this.getClass().getName());
    }

    @GetMapping("/{id}/alerts")
    public ResponseEntity<AppPage<Alert>> alerts(@PathVariable UUID id, Pageable pageable) {
        try {
            AppUser user = appUserService.findById(id).get();
            return ResponseEntity.ok(AppPage.from(appUserService.getUserAlerts(user, pageable)));
        } catch (NoSuchElementException e) {
            return notFound().build();
        }
    }

    @GetMapping
    public AppPage<AppUserDTO> getAllUsers(Pageable pageable) {
        return AppPage.from(appUserService.getAllUsers(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<AppUserDTO> getUserById(@PathVariable UUID id) {
        Optional<AppUserDTO> user = appUserService.getUserById(id);
        return user.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public AppUserDTO createUser(@RequestBody CreateAppUserDTO createUserDTO) {
        return appUserService.createUser(createUserDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<AppUserDTO> updateUser(@PathVariable UUID id, @RequestBody AppUserDTO userDTO) {
        Optional<AppUserDTO> updatedUser = appUserService.updateUser(id, userDTO);
        return updatedUser.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable UUID id) {
        appUserService.deleteUser(id);
        return ResponseEntity.noContent().build();
    }
}
