package com.example.notifffier.api;

import com.example.notifffier.api.dto.PortfolioDTO;
import com.example.notifffier.api.dto.PortfolioValueDTO;
import com.example.notifffier.service.PortfolioService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@CrossOrigin
@RestController
@EnableSpringDataWebSupport
@RequiredArgsConstructor
@RequestMapping("/portfolios")
public class PortfolioController {

    private final PortfolioService portfolioService;

    @GetMapping
    public AppPage<PortfolioDTO> getAllPortfolios(Integer page, Integer size, String[] sort) {
        Pageable pageable = PageRequest.of(page, size, Sort.by(sort));
        return AppPage.from(portfolioService.getAllPortfolios(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<PortfolioDTO> getPortfolioById(@PathVariable UUID id) {
        Optional<PortfolioDTO> portfolio = portfolioService.getPortfolioById(id);
        return portfolio.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping
    public PortfolioDTO createPortfolio(@RequestBody PortfolioDTO portfolioDTO) {
        return portfolioService.createPortfolio(portfolioDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<PortfolioDTO> updatePortfolio(@PathVariable UUID id, @RequestBody PortfolioDTO portfolioDTO) {
        Optional<PortfolioDTO> updatedPortfolio = portfolioService.updatePortfolio(id, portfolioDTO);
        return updatedPortfolio.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePortfolio(@PathVariable UUID id) {
        portfolioService.deletePortfolio(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/user/{userId}")
    public AppPage<PortfolioDTO> getUserPortfolios(
            @PathVariable UUID userId,
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "3", required = false) Integer size,
            @RequestParam(defaultValue = "id", required = false) String[] sort) {
        Pageable pageable = PageRequest.of(page, size, Sort.by(sort));
        return AppPage.from(portfolioService.getUserPortfolios(userId, pageable));
    }

    @GetMapping(value = "/orderedByValue")
    public AppPage<PortfolioValueDTO> getOrderedByValue(
            @RequestParam(defaultValue = "0", required = false) Integer page,
            @RequestParam(defaultValue = "3", required = false) Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        return AppPage.from(portfolioService.getOrderedByValue(pageable));
    }
}
