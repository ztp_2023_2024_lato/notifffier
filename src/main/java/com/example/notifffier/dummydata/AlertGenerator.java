package com.example.notifffier.dummydata;

import com.example.notifffier.domain.Alert;
import com.example.notifffier.domain.AppUser;
import com.example.notifffier.domain.Portfolio;
import com.example.notifffier.domain.monitor.Monitor;
import com.example.notifffier.repository.AlertRepository;
import com.example.notifffier.repository.MonitorRepository;
import com.example.notifffier.repository.PortfolioRepository;
import com.example.notifffier.repository.AppUserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class AlertGenerator {
    private final AlertRepository alertRepository;
    private final MonitorRepository monitorRepository;
    private final Random random = new Random();
    private final AppUserRepository appUserRepository;
    private final PortfolioRepository portfolioRepository;

    @Transactional
    public void generate(int numberOfAlerts) {
        List<Monitor> monitors = new ArrayList<>();
        List<AppUser> users = new ArrayList<>();
        List<Portfolio> portfolios = new ArrayList<>();
        List<Alert> alerts = new ArrayList<>();

        long initialCount = alertRepository.count();

        appUserRepository.findAll().forEach(users::add);
        portfolioRepository.findAll().forEach(portfolios::add);

        monitorRepository.findAll().forEach(monitors::add);

        for (int i = 0; i < numberOfAlerts; i++) {
            Alert alert = new Alert();
            Portfolio portfolio = portfolios.get(random.nextInt(portfolios.size()));
            alert.setPortfolio(portfolio);
            alert.setAppUser(portfolio.getUser());
            alert.setMessage("Alert " + (i + initialCount));
            alert.setMonitor(monitors.get(random.nextInt(monitors.size())));

            alerts.add(alert);
        }

        alertRepository.saveAll(alerts);
    }
}
