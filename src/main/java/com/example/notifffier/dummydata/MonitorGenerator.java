package com.example.notifffier.dummydata;

import com.example.notifffier.domain.AppUser;
import com.example.notifffier.domain.monitor.Monitor;
import com.example.notifffier.domain.monitor.PortfolioPriceMonitor;
import com.example.notifffier.repository.MonitorRepository;
import com.example.notifffier.repository.AppUserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class MonitorGenerator {
    private final MonitorRepository monitorRepository;
    private final AppUserRepository appUserRepository;
    private final Random random = new Random();

    @Transactional
    public void generate(int amountOfMonitors) {
        List<AppUser> users = new ArrayList<>();
        List<Monitor> monitors = new ArrayList<>();

        long initialCount = monitorRepository.count();

        appUserRepository.findAll().forEach(users::add);

        for (int i = 0; i < amountOfMonitors; i++) {
            Monitor monitor = new PortfolioPriceMonitor();
            monitor.setCron("0 0 0 * * *");
            monitor.setDefinedBy(users.get(random.nextInt(users.size())));
            monitor.setName("Monitor " + (i + initialCount));

            monitors.add(monitor);
        }

        monitorRepository.saveAll(monitors);
    }
}
