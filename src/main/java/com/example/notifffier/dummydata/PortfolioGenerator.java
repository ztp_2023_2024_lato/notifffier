package com.example.notifffier.dummydata;

import com.example.notifffier.domain.AppUser;
import com.example.notifffier.domain.Portfolio;
import com.example.notifffier.domain.Product;
import com.example.notifffier.repository.PortfolioRepository;
import com.example.notifffier.repository.ProductRepository;
import com.example.notifffier.repository.AppUserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
@RequiredArgsConstructor
public class PortfolioGenerator {
    private final ProductRepository productRepository;
    private final PortfolioRepository portfolioRepository;
    private final AppUserRepository appUserRepository;
    private final Random random = new Random();

    @Transactional
    public void generate(int numberOfPortfolios) {
        List<Portfolio> portfolios = new ArrayList<>();
        List<Product> products = new ArrayList<>();
        List<AppUser> users = new ArrayList<>();

        long initialCount = portfolioRepository.count();

        productRepository.findAll().forEach(products::add);
        appUserRepository.findAll().forEach(users::add);

        if (products.isEmpty() || users.isEmpty()) {
            return;
        }

        for (int i = 0; i < numberOfPortfolios; i++) {
            Portfolio portfolio = new Portfolio();
            portfolio.setUser(users.get(random.nextInt(users.size())));
            portfolio.setProducts(randomPortfolio(products));
            portfolio.setName("Portfolio " + (i + initialCount));

            portfolios.add(portfolio);
        }

        portfolioRepository.saveAll(portfolios);
    }

    private Map<Product, BigDecimal> randomPortfolio(List<Product> products) {
        Map<Product, BigDecimal> portfolio = new HashMap<>();
        int numberOfProducts = random.nextInt(13);

        for (int i = 0; i < numberOfProducts; i++) {
            Product product = products.get(random.nextInt(products.size()));
            portfolio.put(product, BigDecimal.valueOf(random.nextDouble() * 1000 + 100));
        }

        return portfolio;
    }
}
