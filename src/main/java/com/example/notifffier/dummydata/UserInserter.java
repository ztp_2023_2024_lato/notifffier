package com.example.notifffier.dummydata;

import com.example.notifffier.domain.AppUser;
import com.example.notifffier.repository.AppUserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserInserter {
    private final AppUserRepository appUserRepository;

    @Transactional
    public void generate(int numberOfUsers) {
        List<AppUser> users = new ArrayList<>();

        long initialCount = appUserRepository.count();

        for (int i = 0; i < numberOfUsers; i++) {
            AppUser user = new AppUser();
            user.setUsername("username" + (i + initialCount));
            user.setSurname("surname" + (i + initialCount));
            user.setName("user" + (i + initialCount));
            user.setPasswordHash("password" + (i + initialCount));
            users.add(user);
        }

        appUserRepository.saveAll(users);
    }
}
