package com.example.notifffier.dummydata;

import com.example.notifffier.domain.Product;
import com.example.notifffier.domain.ProductHistoryEntry;
import com.example.notifffier.repository.ProductHistoryRepository;
import com.example.notifffier.repository.ProductRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class ProductInserter {
    private final ProductRepository productRepository;
    private final ProductHistoryRepository productHistoryRepository;
    private final Random random = new Random();

    static final LocalDateTime STARTING_DATE = LocalDateTime.parse("2018-11-30T01:01:01.00");
    static final int NUMBER_OF_DAYS = 100;

    @Transactional
    public void generate(int numberOfProducts) {
        List<Product> products = new ArrayList<>();

        long initialCount = productRepository.count();

        for (int i = 0; i < numberOfProducts; i++) {
            Product product = new Product();
            product.setName("Product " + (i + initialCount));
            product.setFullName("Product " + (i + initialCount));
            product.setSymbol("S" + (i + initialCount));
            product.setCategory("test");
            products.add(product);
        }

        productRepository.saveAll(products);

        products.forEach(this::generateHistory);
    }

    private void generateHistory(Product product) {
        List<ProductHistoryEntry> history = new ArrayList<>();

        double trend = random.nextDouble() - 0.3;
        trend = trend/100 + 1;

        double open = Math.max(100, random.nextDouble() * 1000);
        double low = open - random.nextDouble() * 20-20;
        double high = open - random.nextDouble() * 20+20;
        double close = Math.max(low, Math.min(open * trend * 10, high));
        double volume = 0;
        double openInterest = 0;

        for (int i = 0; i < NUMBER_OF_DAYS; i++) {
            ProductHistoryEntry entry = new ProductHistoryEntry();
            entry.setProduct(product);
            entry.setTimestamp(STARTING_DATE.plusDays(i));
            entry.setVolume(volume);
            entry.setOpen(open);
            entry.setClose(close);
            entry.setLow(low);
            entry.setHigh(high);
            entry.setOpenInterest(openInterest);
            history.add(entry);

            open = Math.max(0, close + random.nextDouble() * 10 - 5);
            low = Math.max(0, open - random.nextDouble() * 20-20);
            high = Math.max(0, open - random.nextDouble() * 20+20);
            close = Math.max(0, Math.max(low, Math.min(open * trend , high)));
        }

        productHistoryRepository.saveAll(history);
    }
}
