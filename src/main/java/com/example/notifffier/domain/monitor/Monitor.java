package com.example.notifffier.domain.monitor;

import com.example.notifffier.domain.Alert;
import com.example.notifffier.domain.AppUser;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@Access(AccessType.FIELD)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Monitor {
    @Id
    @GeneratedValue
    private UUID id;

    String name;

    String cron;

    @ManyToOne
    AppUser definedBy;

    public abstract List<Alert> generate(List<AppUser> users);
}
