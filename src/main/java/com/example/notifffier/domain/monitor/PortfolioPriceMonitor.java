package com.example.notifffier.domain.monitor;

import com.example.notifffier.domain.Alert;
import com.example.notifffier.domain.AppUser;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

import java.util.ArrayList;
import java.util.List;

@Entity
public class PortfolioPriceMonitor extends Monitor {
    public static final String DTYPE = "PortfolioPriceMonitor";
    @Override
    public List<Alert> generate(List<AppUser> users) {
        return new ArrayList<>();
    }
}
