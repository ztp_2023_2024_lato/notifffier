package com.example.notifffier.domain;

import com.example.notifffier.domain.monitor.Monitor;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "alert")
public class Alert {
    @Id
    @GeneratedValue
    private UUID id;

    @ManyToOne
    AppUser appUser;

    @ManyToOne
    Monitor monitor;

    String message;

    @ManyToOne
    Product product;

    @ManyToOne
    Portfolio portfolio;
}
