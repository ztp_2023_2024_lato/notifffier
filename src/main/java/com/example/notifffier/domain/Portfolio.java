package com.example.notifffier.domain;

import com.example.notifffier.api.dto.PortfolioValueDTO;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Map;
import java.util.UUID;

@Entity
@Getter
@Setter
@Access(AccessType.FIELD)
@NamedNativeQuery(
        name = "PortfolioValues",
        query = """
                select portfolio_id, sum(pp.quantity * pv.close) portfolio_value from portfolio_products pp
                              join (select phe.product_id,
                                      phe.close,
                                      ROW_NUMBER() over (partition by phe.product_id order by phe.date) row_number
                               from product_history_entry phe) pv
                              on pv.product_id = pp.products_key and row_number = 1
                group by portfolio_id
                order by portfolio_value desc
        """,
        resultSetMapping = "PortfolioValueDTO"
)
@SqlResultSetMapping(
        name = "PortfolioValueDTO",
        classes = @ConstructorResult(
                targetClass = PortfolioValueDTO.class,
                columns = {
                        @ColumnResult(name = "portfolio_id", type = Long.class),
                        @ColumnResult(name = "portfolio_value", type = BigDecimal.class)
                }
        )
)
public class Portfolio {

    @Id
    @GeneratedValue
    private UUID id;

    @ElementCollection
    @Column(name = "quantity")
    private Map<Product, BigDecimal> products;

    @ManyToOne
    private AppUser user;

    private String name;
}
