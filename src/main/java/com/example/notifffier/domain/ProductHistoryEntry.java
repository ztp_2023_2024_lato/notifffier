package com.example.notifffier.domain;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@Setter
@Access(AccessType.FIELD)
@With
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "product_history_entry")
public class ProductHistoryEntry {
    @Id
    @GeneratedValue
    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY)
    Product product;

    LocalDateTime timestamp;

    double open;

    double high;

    double low;

    double close;

    double volume;

    double openInterest;
}
