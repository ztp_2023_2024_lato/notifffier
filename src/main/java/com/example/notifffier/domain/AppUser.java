package com.example.notifffier.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Entity
@Getter
@Setter
@Access(AccessType.FIELD)
@Table(name = "APP_USER")
public class AppUser {
    @Id
    @GeneratedValue
    private UUID id;

    String name;

    String surname;

    String username;

    String passwordHash;
}
