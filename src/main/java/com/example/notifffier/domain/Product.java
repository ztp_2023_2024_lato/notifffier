package com.example.notifffier.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Entity
@Getter
@Setter
@Access(AccessType.FIELD)
public class Product {
    @Id
    @GeneratedValue
    private UUID id;

    @Column(unique = true)
    String symbol;

    String name;

    String fullName;

    String category;
}
