package com.example.notifffier.util;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class Symbol {
    private final String symbol;
    private final String name;
    private final String fullName;
    private final String category;

}
