package com.example.notifffier.util;

import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SymbolParser {

    public static List<Symbol> parseSymbolData(String htmlContent) {
        List<Symbol> symbolData = new ArrayList<>();

        // Parse HTML content using Jsoup
        Document doc = Jsoup.parse(htmlContent);

        // Select the table body element
        Element symbolTable = doc.select("tbody#f13").first();

        // If table body element is found
        if (symbolTable != null) {
            // Select all table rows
            Elements symbolRows = symbolTable.getElementsByTag("tr");

            // Iterate through each row, starting from index 1
            for (int i = 1; i < symbolRows.size(); i++) {
                Element symbolRow = symbolRows.get(i);
                // Extract data from table cells
                String symbolName = symbolRow.getElementsByTag("td").get(2).text();
                String name = symbolRow.getElementsByTag("td").get(3).text();
                String fullName = symbolRow.getElementsByTag("td").get(4).text();
                String category = symbolRow.getElementsByTag("td").get(5).text();

                // Create Symbol object and add to symbolData list
                Symbol symbol = new Symbol(symbolName, name, fullName, category);
                symbolData.add(symbol);
            }
        }

        return symbolData;
    }
}
