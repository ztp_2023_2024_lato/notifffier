package com.example.notifffier.util;

import java.io.IOException;
import java.util.List;

import lombok.RequiredArgsConstructor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SymbolDataFetcher {

    private static final String BASE_URL = "https://stooq.pl/q/s/?q=";
    private static final String PAGE_PARAM = "&l=1";

    public List<Symbol> fetchAndParseSymbolData(String symbol) throws IOException {
        // Construct URL
        String url = BASE_URL + symbol + PAGE_PARAM;

        // Fetch HTML content from URL
        Document doc = Jsoup.connect(url).cookie("cookie_uu", "240403000").cookie("FCCDCF",
                "%5Bnull%2Cnull%2Cnull%2C%5B%22CP7-kIAP7-kIAEsACBPLAtEoAP_gAEPgAB5YINJD7D7FbSFCwH5zaLsAMAhHRsCAQoQAAASBAmABQAKQIAQCgkAQFASgBAACAAAAICZBIQIECAAACUAAQAAAAAAEAAAAAAAIIAAAgAEAAAAIAAACAAAAEAAIAAAAEAAAmAgAAIIACAAAhAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAQOhQD2F2K2kKFkPCmQWYAQBCijYEAhQAAAAkCBIAAgAUgQAgFIIAgAIFAAAAAAAAAQEgCQAAQABAAAIACgAAAAAAIAAAAAAAQQAAAAAIAAAAAAAAEAAAAAAAQAAAAIAABEhCAAQQAEAAAAAAAQAAAAAAAAAAABAAA.f-gAAAAAAAA%22%2C%222~2072.70.89.93.108.122.149.196.2253.2299.259.2357.311.313.…211.1276.1301.1365.1415.1423.1449.1570.1577.1598.1651.1716.1735.1753.1765.1870.1878.1889.1958~dv.%22%2C%22447A630E-B9FF-47AA-9CAC-4841BD7E2866%22%5D%5D")
                .cookie("FCNEC",
                        "%5B%5B%22AKsRol_Zdv4CKOn6pZ-9zHXT_t5MSa4BUw2pxTqAdMiP26tJtXKACbyNWQedFfk66D2KNI65kvJWi1KhV3zFUPunvqOs6zmGiWYRCQ4jOjclabHyxGBw0wSFrOirTPsi5SVGLpY7mqV7FvqZcfOEgVjtNeoT9FRfog%3D%3D%22%5D%5D")
                .cookie("privacy", "1711274495").cookie("PHPSESSID", "q49dkshr7hp4k19er2rdknemb3")
                .cookie("uid", "pl7ed317c7uoztprcxlz08hd2c")
                .cookie("cookie_user", "%3F0005mllg000011100d1300e3%7C4817.n+fw20%7E00006enrka010ej0ew").get();

        // Parse HTML content using SymbolParser
        return SymbolParser.parseSymbolData(doc.html());
    }
}
