package com.example.notifffier.repository;

import com.example.notifffier.domain.ProductHistoryEntry;
import org.springframework.data.repository.CrudRepository;

public interface ProductHistoryRepository extends CrudRepository<ProductHistoryEntry, java.util.UUID>, CustomProductHistoryRepository {

}