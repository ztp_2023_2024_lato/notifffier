package com.example.notifffier.repository;

import com.example.notifffier.domain.ProductHistoryEntry;

import java.util.List;
import java.util.UUID;

public interface CustomProductHistoryRepository {
    List<ProductHistoryEntry> findLatestByProductId(List<UUID> productId);
}
