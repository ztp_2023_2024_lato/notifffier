package com.example.notifffier.repository;

import com.example.notifffier.api.dto.PortfolioValueDTO;
import com.example.notifffier.domain.Portfolio;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.UUID;

public interface CustomPortfolioRepository {
    Page<Portfolio> findAllByUserId(UUID userId, Pageable pageable);

    Optional<UUID> findUserWithMostPortfolios();

    Page<PortfolioValueDTO> findAllOderedByValueDesc(Pageable pageable);
}
