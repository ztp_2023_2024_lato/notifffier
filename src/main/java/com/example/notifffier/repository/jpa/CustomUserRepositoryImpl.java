package com.example.notifffier.repository.jpa;

import com.example.notifffier.domain.AppUser;
import com.example.notifffier.repository.CustomUserRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

import java.util.Optional;

public class CustomUserRepositoryImpl implements CustomUserRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<AppUser> findByUsername(String username) {
        return entityManager.createQuery("""
                SELECT u from AppUser u where u.username=:username
                """, AppUser.class).setParameter("username", username)
                .getResultStream()
                .findFirst();
    }
}
