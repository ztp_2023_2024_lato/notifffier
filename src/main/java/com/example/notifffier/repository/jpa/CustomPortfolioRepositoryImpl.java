package com.example.notifffier.repository.jpa;

import com.example.notifffier.api.dto.PortfolioValueDTO;
import com.example.notifffier.domain.Portfolio;
import com.example.notifffier.repository.AppUserRepository;
import com.example.notifffier.repository.CustomPortfolioRepository;
import jakarta.persistence.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
@RequiredArgsConstructor
public class CustomPortfolioRepositoryImpl implements CustomPortfolioRepository {
    @PersistenceContext
    private EntityManager entityManager;

    private final AppUserRepository appUserRepository;

    @Override
    public Page<Portfolio> findAllByUserId(UUID userId, Pageable pageable) {
        String jpql = "SELECT p FROM Portfolio p WHERE p.user.id = :userId";
        TypedQuery<Portfolio> query = entityManager.createQuery(jpql, Portfolio.class);
        query.setParameter("userId", userId);
        query.setFirstResult((int) pageable.getOffset());
        query.setMaxResults(pageable.getPageSize());

        List<Portfolio> portfolios = query.getResultList();
        long total = countByUserId(userId);

        return new PageImpl<>(portfolios, pageable, total);
    }

    @Override
    public Optional<UUID> findUserWithMostPortfolios() {
        String sql = "select user_id from portfolio group by user_id order by count(id) desc fetch first 1 row only";
        Query query = entityManager.createNativeQuery(sql);

        return query.getResultStream()
                .findFirst()
                .map(CustomPortfolioRepositoryImpl::convertToUUID);
    }

    public static UUID convertToUUID(Object o) {
        if (o.getClass().equals(UUID.class)) {
            return (UUID) o;
        } else if (o instanceof byte[]) {
            ByteBuffer byteBuffer = ByteBuffer.wrap((byte[]) o);
            long high = byteBuffer.getLong();
            long low = byteBuffer.getLong();
            return new UUID(high, low);
        } else {
            throw new IllegalArgumentException("could not convert object to UUID, class name: " + o.getClass().getName());
        }
    }

    @Override
    public Page<PortfolioValueDTO> findAllOderedByValueDesc(Pageable pageable) {
        List<PortfolioValueDTO> portfolios = entityManager.createNamedQuery("PortfolioValues", PortfolioValueDTO.class)
                .setFirstResult((int) pageable.getOffset())
                .setMaxResults(pageable.getPageSize())
                .getResultList();

        long total = appUserRepository.count();

        return new PageImpl<>(portfolios, pageable, total);
    }

    private long countByUserId(UUID userId) {
        String countJpql = "SELECT COUNT(p) FROM Portfolio p WHERE p.user.id = :userId";
        TypedQuery<Long> countQuery = entityManager.createQuery(countJpql, Long.class);
        countQuery.setParameter("userId", userId);
        return countQuery.getSingleResult();
    }
}
