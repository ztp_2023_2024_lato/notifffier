package com.example.notifffier.repository.jpa;

import com.example.notifffier.domain.ProductHistoryEntry;
import com.example.notifffier.repository.CustomProductHistoryRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;

import java.util.List;
import java.util.UUID;

public class CustomProductHistoryRepositoryImpl implements CustomProductHistoryRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public List<ProductHistoryEntry> findLatestByProductId(List<UUID> productId) {
        String jpql = "SELECT p FROM ProductHistoryEntry p WHERE p.product.id in (:productId) ORDER BY p.timestamp DESC";
        TypedQuery<ProductHistoryEntry> query = entityManager.createQuery(jpql, ProductHistoryEntry.class);
        query.setParameter("productId", productId);
        return query.getResultList();
    }
}

