package com.example.notifffier.repository;

import com.example.notifffier.domain.AppUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.UUID;

public interface AppUserRepository
        extends CrudRepository<AppUser, UUID>, PagingAndSortingRepository<AppUser, java.util.UUID>, CustomUserRepository {
}
