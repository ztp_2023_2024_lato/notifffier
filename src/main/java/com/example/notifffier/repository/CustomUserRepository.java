package com.example.notifffier.repository;

import com.example.notifffier.domain.Alert;
import com.example.notifffier.domain.AppUser;

import java.util.List;
import java.util.Optional;

public interface CustomUserRepository {
    public Optional<AppUser> findByUsername(String username);
}
