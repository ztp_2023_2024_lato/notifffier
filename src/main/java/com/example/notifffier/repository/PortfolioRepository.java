package com.example.notifffier.repository;

import com.example.notifffier.domain.Portfolio;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.UUID;

public interface PortfolioRepository extends CrudRepository<Portfolio, UUID>, PagingAndSortingRepository<Portfolio, UUID>, CustomPortfolioRepository {

}
