package com.example.notifffier.repository;

import com.example.notifffier.domain.Alert;
import com.example.notifffier.domain.AppUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AlertRepository extends CrudRepository<Alert, java.util.UUID>, PagingAndSortingRepository<Alert, java.util.UUID> {
    Page<Alert> findAllByAppUser(AppUser user, Pageable pageable);
}
