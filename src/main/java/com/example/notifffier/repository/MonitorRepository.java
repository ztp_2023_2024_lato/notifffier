package com.example.notifffier.repository;

import com.example.notifffier.domain.monitor.Monitor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.UUID;

public interface MonitorRepository extends CrudRepository<Monitor, UUID>, PagingAndSortingRepository<Monitor, UUID> {
}
