package com.example.notifffier.repository;

import com.example.notifffier.domain.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;
import java.util.UUID;

public interface ProductRepository extends CrudRepository<Product, UUID>, PagingAndSortingRepository<Product, UUID> {
    public Optional<Product> findBySymbol(String name);
}
