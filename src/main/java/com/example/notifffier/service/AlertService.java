package com.example.notifffier.service;

import com.example.notifffier.api.dto.AlertDTO;
import com.example.notifffier.domain.*;
import com.example.notifffier.repository.*;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@Service
@EnableScheduling
public class AlertService {

    private final AlertRepository alertRepository; 
    private final AppUserRepository appUserRepository;
    private final MonitorRepository monitorRepository; 
    private final ProductRepository productRepository; 
    private final PortfolioRepository portfolioRepository;

    public Iterable<Alert> findAll() {
        return alertRepository.findAll();
    }

    public Page<Alert> findAll(Pageable pageable) {
        return alertRepository.findAll(pageable);
    }

    public Page<AlertDTO> getAllAlerts(Pageable pageable) {
        return alertRepository.findAll(pageable)
                .map(this::convertToDTO);
    }

    public Optional<AlertDTO> getAlertById(UUID id) {
        return alertRepository.findById(id)
                .map(this::convertToDTO);
    }

    @Transactional
    public Optional<AlertDTO> updateAlert(UUID id, AlertDTO alertDTO) {
        return alertRepository.findById(id).map(alert -> {
            alert.setMessage(alertDTO.getMessage());
            alert.setAppUser(appUserRepository.findById(alertDTO.getAppUserId()).orElse(null));
            alert.setMonitor(monitorRepository.findById(alertDTO.getMonitorId()).orElse(null));
            alert.setProduct(productRepository.findById(alertDTO.getProductId()).orElse(null));
            alert.setPortfolio(portfolioRepository.findById(alertDTO.getPortfolioId()).orElse(null));
            Alert updatedAlert = alertRepository.save(alert);
            return convertToDTO(updatedAlert);
        });
    }

    @Transactional
    public void deleteAlert(UUID id) {
        alertRepository.deleteById(id);
    }

    private AlertDTO convertToDTO(Alert alert) {
        AlertDTO dto = new AlertDTO();
        dto.setId(alert.getId());
        dto.setMessage(alert.getMessage());
        dto.setAppUserId(alert.getAppUser().getId());
        dto.setMonitorId(alert.getMonitor().getId());
        dto.setProductId(alert.getProduct().getId());
        dto.setPortfolioId(alert.getPortfolio().getId());
        return dto;
    }
}
