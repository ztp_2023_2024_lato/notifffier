package com.example.notifffier.service;

import com.example.notifffier.api.dto.PortfolioDTO;
import com.example.notifffier.api.dto.PortfolioValueDTO;
import com.example.notifffier.domain.AppUser;
import com.example.notifffier.domain.Portfolio;
import com.example.notifffier.repository.PortfolioRepository;
import com.example.notifffier.repository.ProductRepository;
import com.example.notifffier.repository.AppUserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PortfolioService {

    private final PortfolioRepository portfolioRepository;

    private final ProductRepository productRepository;

    private final AppUserRepository appUserRepository;

    public Page<PortfolioDTO> getAllPortfolios(Pageable pageable) {
        return portfolioRepository.findAll(pageable)
                .map(this::convertToDTO);
    }

    public Page<PortfolioValueDTO> getOrderedByValue(Pageable pageable) {
        return portfolioRepository.findAllOderedByValueDesc(pageable);
    }

    public Optional<PortfolioDTO> getPortfolioById(UUID id) {
        return portfolioRepository.findById(id)
                .map(this::convertToDTO);
    }

    @Transactional
    public PortfolioDTO createPortfolio(PortfolioDTO portfolioDTO) {
        Portfolio portfolio = convertToEntity(portfolioDTO);
        Portfolio savedPortfolio = portfolioRepository.save(portfolio);
        return convertToDTO(savedPortfolio);
    }

    @Transactional
    public Optional<PortfolioDTO> updatePortfolio(UUID id, PortfolioDTO portfolioDTO) {
        AppUser user = appUserRepository.findById(portfolioDTO.getUserId()).orElseThrow();

        return portfolioRepository.findById(id).map(portfolio -> {
            portfolio.setName(portfolioDTO.getName());
            portfolio.setUser(user); // Assuming a constructor in AppUser
            portfolio.setProducts(portfolioDTO.getProductIds().entrySet().stream()
                    .collect(Collectors.toMap(
                            entry -> productRepository.findById(entry.getKey()).orElse(null),
                            Map.Entry::getValue)));
            Portfolio updatedPortfolio = portfolioRepository.save(portfolio);
            return convertToDTO(updatedPortfolio);
        });
    }

    @Transactional
    public void deletePortfolio(UUID id) {
        portfolioRepository.deleteById(id);
    }

    private PortfolioDTO convertToDTO(Portfolio portfolio) {
        PortfolioDTO dto = new PortfolioDTO();
        dto.setId(portfolio.getId());
        dto.setName(portfolio.getName());
        dto.setUserId(portfolio.getUser().getId());
        dto.setProductIds(portfolio.getProducts().entrySet().stream()
                .collect(Collectors.toMap(
                        entry -> entry.getKey().getId(),
                        Map.Entry::getValue)));
        return dto;
    }

    private Portfolio convertToEntity(PortfolioDTO portfolioDTO) {
        AppUser user = appUserRepository.findById(portfolioDTO.getUserId()).orElseThrow();

        Portfolio portfolio = new Portfolio();
        portfolio.setId(portfolioDTO.getId());
        portfolio.setName(portfolioDTO.getName());
        portfolio.setUser(user);
        portfolio.setProducts(portfolioDTO.getProductIds().entrySet().stream()
                .collect(Collectors.toMap(
                        entry -> productRepository.findById(entry.getKey()).orElse(null),
                        Map.Entry::getValue)));
        return portfolio;
    }

    public Iterable<Portfolio> findAll() {
        return portfolioRepository.findAll();
    }

    public Page<PortfolioDTO> getUserPortfolios(UUID userId, Pageable pageable) {
        return portfolioRepository.findAllByUserId(userId, pageable).map(this::convertToDTO);
    }
}
