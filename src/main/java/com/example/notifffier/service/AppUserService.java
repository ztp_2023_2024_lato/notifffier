package com.example.notifffier.service;

import com.example.notifffier.api.dto.AppUserDTO;
import com.example.notifffier.api.dto.CreateAppUserDTO;
import com.example.notifffier.domain.Alert;
import com.example.notifffier.domain.AppUser;
import com.example.notifffier.repository.AlertRepository;
import com.example.notifffier.repository.AppUserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AppUserService {
    private final AppUserRepository appUserRepository;
    private final AlertRepository alertRepository;
    private final PasswordEncoder passwordEncoder;

    public AppUser getByUsername(String username) {
        return appUserRepository.findByUsername(username).orElse(null);
    }

    public Optional<AppUser> findById(UUID id) {
        return appUserRepository.findById(id);
    }

    public Page<Alert> getUserAlerts(AppUser user, Pageable pageable) {
        return alertRepository.findAllByAppUser(user, pageable);
    }

    public Page<AppUserDTO> getAllUsers(Pageable pageable) {
        return appUserRepository.findAll(pageable)
                .map(this::convertToDTO);
    }

    public Optional<AppUserDTO> getUserById(UUID id) {
        return appUserRepository.findById(id)
                .map(this::convertToDTO);
    }

    @Transactional
    public AppUserDTO createUser(CreateAppUserDTO createUserDTO) {
        AppUser user = new AppUser();
        user.setName(createUserDTO.getName());
        user.setSurname(createUserDTO.getSurname());
        user.setUsername(createUserDTO.getUsername());
        user.setPasswordHash(passwordEncoder.encode(createUserDTO.getPassword()));
        AppUser savedUser = appUserRepository.save(user);
        return convertToDTO(savedUser);
    }

    @Transactional
    public Optional<AppUserDTO> updateUser(UUID id, AppUserDTO userDTO) {
        return appUserRepository.findById(id).map(user -> {
            user.setName(userDTO.getName());
            user.setSurname(userDTO.getSurname());
            user.setUsername(userDTO.getUsername());
            AppUser updatedUser = appUserRepository.save(user);
            return convertToDTO(updatedUser);
        });
    }

    @Transactional
    public void deleteUser(UUID id) {
        appUserRepository.deleteById(id);
    }

    private AppUserDTO convertToDTO(AppUser user) {
        AppUserDTO dto = new AppUserDTO();
        dto.setId(user.getId());
        dto.setName(user.getName());
        dto.setSurname(user.getSurname());
        dto.setUsername(user.getUsername());
        return dto;
    }

    public Iterable<AppUser> findAll() {
        return appUserRepository.findAll();
    }
}
