package com.example.notifffier.service;

import com.example.notifffier.domain.Product;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import java.util.*;

@Service
@RequestScope
@RequiredArgsConstructor
public class ProductNameCache {
    private final ProductService service;
    private Map<String, Product> productsCache = new HashMap<>();

    @PostConstruct
    void refresh() {
        service.findAll().forEach(product -> {
            productsCache.put(product.getSymbol(), product);
        });
    }

    public Optional<Product> findByName(String name) {
        return Optional.ofNullable(productsCache.get(name));
    }

    public boolean isCached(Product product) {
        return productsCache.containsKey(product.getSymbol());
    }
}
