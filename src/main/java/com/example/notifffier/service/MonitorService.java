package com.example.notifffier.service;

import com.example.notifffier.api.dto.CreateMonitorDTO;
import com.example.notifffier.api.dto.MonitorDTO;
import com.example.notifffier.domain.monitor.Monitor;
import com.example.notifffier.domain.monitor.PortfolioPriceMonitor;
import com.example.notifffier.repository.MonitorRepository;
import com.example.notifffier.repository.AppUserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class MonitorService {
    private final MonitorRepository monitorRepository;
    private final AppUserRepository appUserRepository;

    public Page<Monitor> findAll(Pageable pageable) {
        return monitorRepository.findAll(pageable);
    }


    public Page<MonitorDTO> getAllMonitors(Pageable pageable) {
        return monitorRepository.findAll(pageable)
                .map(this::convertToDTO);
    }

    public Optional<MonitorDTO> getMonitorById(UUID id) {
        return monitorRepository.findById(id)
                .map(this::convertToDTO);
    }

    @Transactional
    public MonitorDTO createMonitor(CreateMonitorDTO createMonitorDTO) {
        Monitor monitor = convertToEntity(createMonitorDTO);
        Monitor savedMonitor = monitorRepository.save(monitor);
        return convertToDTO(savedMonitor);
    }

    @Transactional
    public Optional<MonitorDTO> updateMonitor(UUID id, MonitorDTO monitorDTO) {
        return monitorRepository.findById(id).map(monitor -> {
            monitor.setName(monitorDTO.getName());
            monitor.setCron(monitorDTO.getCron());
            monitor.setDefinedBy(appUserRepository.findById(monitorDTO.getDefinedById()).orElse(null));
            Monitor updatedMonitor = monitorRepository.save(monitor);
            return convertToDTO(updatedMonitor);
        });
    }

    @Transactional
    public void deleteMonitor(UUID id) {
        monitorRepository.deleteById(id);
    }

    private MonitorDTO convertToDTO(Monitor monitor) {
        MonitorDTO dto = new MonitorDTO();
        dto.setId(monitor.getId());
        dto.setName(monitor.getName());
        dto.setCron(monitor.getCron());
        dto.setDefinedById(monitor.getDefinedBy().getId());
        return dto;
    }

    private Monitor convertToEntity(CreateMonitorDTO dto) {
        switch (dto.getType()) {
            case PortfolioPriceMonitor.DTYPE:
                Monitor monitor = new PortfolioPriceMonitor();
                monitor.setName(dto.getName());
                monitor.setCron(dto.getCron());
                monitor.setDefinedBy(appUserRepository.findById(dto.getDefinedById()).orElse(null));
                return monitor;
            default:
                throw new IllegalStateException();
        }
    }
}
