package com.example.notifffier.service;

import com.example.notifffier.domain.Product;
import com.example.notifffier.domain.ProductHistoryEntry;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class ProductCsvReader {
    private final String DATA_HEADER = "<TICKER>,<PER>,<DATE>,<TIME>,<OPEN>,<HIGH>,<LOW>,<CLOSE>,<VOL>,<OPENINT>";

    public static void read(MultipartFile file, ProductService service) throws IOException {

        CsvMapper mapper = new CsvMapper();
        CsvSchema schema = mapper.schemaFor(ProductEntry.class).withHeader().withColumnReordering(true);

        ObjectReader reader = mapper.readerFor(ProductEntry.class).with(schema);

        List<ProductEntry> entries = reader.<ProductEntry> readValues(file.getInputStream()).readAll();

        List<ProductHistoryEntry> historyEntries = entries.stream().map((entry) -> {
            ProductHistoryEntry historyEntry = new ProductHistoryEntry();
            Product product = service.findByNameOrFetch(entry.ticker).orElseThrow();
            historyEntry.setProduct(product);
            historyEntry.setTimestamp(
                    LocalDateTime.parse(entry.date + " " + entry.time, DateTimeFormatter.ofPattern("yyyyMMdd HHmmss")));
            historyEntry.setOpen(entry.open.doubleValue());
            historyEntry.setHigh(entry.high.doubleValue());
            historyEntry.setLow(entry.low.doubleValue());
            historyEntry.setClose(entry.close.doubleValue());
            historyEntry.setVolume(entry.vol.doubleValue());
            historyEntry.setOpenInterest(entry.openint.doubleValue());
            return historyEntry;
        }).toList();

        service.saveAll(historyEntries);
    }

    private static class ProductEntry {
        @JsonProperty("<TICKER>")
        public String ticker;
        @JsonProperty("<PER>")
        public String per;
        @JsonProperty("<DATE>")
        public String date;
        @JsonProperty("<TIME>")
        public String time;
        @JsonProperty("<OPEN>")
        public BigDecimal open;
        @JsonProperty("<HIGH>")
        public BigDecimal high;
        @JsonProperty("<LOW>")
        public BigDecimal low;
        @JsonProperty("<CLOSE>")
        public BigDecimal close;
        @JsonProperty("<VOL>")
        public BigDecimal vol;
        @JsonProperty("<OPENINT>")
        public BigDecimal openint;
    }
}
