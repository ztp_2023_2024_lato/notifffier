package com.example.notifffier.service;

import com.example.notifffier.api.dto.ProductDTO;
import com.example.notifffier.api.dto.ProductHistoryEntryDTO;
import com.example.notifffier.domain.Product;
import com.example.notifffier.domain.ProductHistoryEntry;
import com.example.notifffier.repository.CustomProductHistoryRepository;
import com.example.notifffier.repository.ProductHistoryRepository;
import com.example.notifffier.repository.ProductRepository;
import com.example.notifffier.util.SymbolDataFetcher;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Predicate;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductHistoryRepository historyRepository;
    private final ProductRepository productRepository;
    private final SymbolDataFetcher symbolDataFetcher;

    @Autowired
    ProductNameCache productNameCache;
    @Autowired
    private CustomProductHistoryRepository productHistoryRepository;

    public Optional<Product> findByNameOrFetch(String symbol) {
        Optional<Product> product = productNameCache.findByName(symbol);

        if (product.isEmpty()) {
            try {
                List<Product> products = symbolDataFetcher.fetchAndParseSymbolData(symbol).stream().map(s -> {
                    Product p = new Product();
                    p.setSymbol(s.getSymbol());
                    p.setName(s.getName());
                    p.setFullName(s.getFullName());
                    p.setCategory(s.getCategory());
                    return p;
                }).filter(Predicate.not(productNameCache::isCached)).toList();

                productRepository.saveAll(products);
                productNameCache.refresh();
                product = productRepository.findBySymbol(symbol);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        return product;
    }

    public void saveAll(Iterable<ProductHistoryEntry> products) {
        historyRepository.saveAll(products);
    }

    public Iterable<Product> findAll() {
        return productRepository.findAll();
    }

    public Page<ProductDTO> getAllProducts(Pageable pageable) {
        return productRepository.findAll(pageable)
                .map(this::convertToDTO);
    }

    public Optional<ProductDTO> getProductById(UUID id) {
        return productRepository.findById(id)
                .map(this::convertToDTO);
    }

    @Transactional
    public Optional<ProductDTO> updateProduct(UUID id, ProductDTO productDTO) {
        return productRepository.findById(id).map(product -> {
            product.setSymbol(productDTO.getSymbol());
            product.setName(productDTO.getName());
            product.setFullName(productDTO.getFullName());
            product.setCategory(productDTO.getCategory());
            Product updatedProduct = productRepository.save(product);
            return convertToDTO(updatedProduct);
        });
    }

    @Transactional
    public void deleteProduct(UUID id) {
        productRepository.deleteById(id);
    }

    private ProductDTO convertToDTO(Product product) {
        ProductDTO dto = new ProductDTO();
        dto.setId(product.getId());
        dto.setSymbol(product.getSymbol());
        dto.setName(product.getName());
        dto.setFullName(product.getFullName());
        dto.setCategory(product.getCategory());
        return dto;
    }

    public List<Product> getProductsByIds(List<UUID> ids) {
        List<Product> products = new ArrayList<>();
        productRepository.findAllById(ids).forEach(products::add);
        return products;
    }

    public List<ProductHistoryEntryDTO> getLatestProductHistories(List<UUID> productIds) {
        return productHistoryRepository.findLatestByProductId(productIds)
                .stream()
                .map(this::convertToDTO)
                .toList();
    }

    private ProductHistoryEntryDTO convertToDTO(ProductHistoryEntry entry) {
        ProductHistoryEntryDTO dto = new ProductHistoryEntryDTO();
        dto.setId(entry.getId());
        dto.setProductId(entry.getProduct().getId());
        dto.setDate(entry.getTimestamp());
        dto.setOpen(entry.getOpen());
        dto.setHigh(entry.getHigh());
        dto.setLow(entry.getLow());
        dto.setClose(entry.getClose());
        dto.setVolume(entry.getVolume());
        dto.setOpenInterest(entry.getOpenInterest());
        return dto;
    }
}
